int MultiplyTest(Epetra_MpiComm &comm, std::ostream &out, int outputVerbosity, std::string params_file){

	int err; // for implementing functions

	// get param list
	Teuchos::RCP< Teuchos::ParameterList > pl = Teuchos::getParametersFromXmlFile(params_file);
	int m = pl->get<int>("m");
	int n = pl->get<int>("n");
	int nframes = pl->get<int>("nframes");
	int nlayers = pl->get<int>("nlayers");
	int ssp = pl->get<int>("ssp");
	const char* file_W_out_var = (pl->get<std::string>("file_W_out_var")).c_str();
	const char* file_R_out_var = (pl->get<std::string>("file_R_out_var")).c_str();
	const char* file_A_out_var = (pl->get<std::string>("file_A_out_var")).c_str();
	const char* file_W_in = (pl->get<std::string>("file_W_in")).c_str();
	const char* file_R_in = (pl->get<std::string>("file_R_in")).c_str();
	const char* file_A_in = (pl->get<std::string>("file_A_in")).c_str();
	const char* file_W_out = (pl->get<std::string>("file_W_out")).c_str();
	const char* file_R_out = (pl->get<std::string>("file_R_out")).c_str();
	const char* file_A_out = (pl->get<std::string>("file_A_out")).c_str();
	const char* file_deltax = (pl->get<std::string>("file_deltax")).c_str();
	const char* file_deltay = (pl->get<std::string>("file_deltay")).c_str();
	const char* file_W_unmod = (pl->get<std::string>("file_W_unmod")).c_str();
	const char* file_W_unmod_var = (pl->get<std::string>("file_W_unmod_var")).c_str();
	const char* file_pupil_mask = (pl->get<std::string>("file_pupil_mask")).c_str();

	const char* file_DxDy_in = (pl->get<std::string>("file_DxDy_in")).c_str();
	const char* file_Dx_out = (pl->get<std::string>("file_Dx_out")).c_str();
	const char* file_Dy_out = (pl->get<std::string>("file_Dy_out")).c_str();
	const char* file_Dx_out_var = (pl->get<std::string>("file_Dx_out_var")).c_str();
	const char* file_Dy_out_var = (pl->get<std::string>("file_Dy_out_var")).c_str();

	const char* file_Pad_in = (pl->get<std::string>("file_Pad_in")).c_str();
	const char* file_Pad_out = (pl->get<std::string>("file_Pad_out")).c_str();
	const char* file_Pad_out_var = (pl->get<std::string>("file_Pad_out_var")).c_str();

	const char* file_PhaseRecon_in = (pl->get<std::string>("file_PhaseRecon_in")).c_str();
	const char* file_PhaseRecon_out = (pl->get<std::string>("file_PhaseRecon_out")).c_str();
	const char* file_PhaseRecon_out_var = (pl->get<std::string>("file_PhaseRecon_out_var")).c_str();

	const char* file_PhasePupil_in = (pl->get<std::string>("file_PhasePupil_in")).c_str();
	const char* file_PhasePupil_out = (pl->get<std::string>("file_PhasePupil_out")).c_str();
	const char* file_PhasePupil_out_var = (pl->get<std::string>("file_PhasePupil_out_var")).c_str();





	double deltax[nlayers];
	double deltay[nlayers];
	double *vals = new double[m*n];

	err = ReadMatAsStdVector(file_deltax,  deltax, nlayers);
	err = ReadMatAsStdVector(file_deltay,  deltay, nlayers);
	err = ReadMatAsStdVector(file_pupil_mask,  vals, m*n);

	cout << deltax[0] << endl;

	// Get our processor ID (0 if running serial)
	int MyPID = comm.MyPID();
	// Verbose flag: only processor 0 should print
	bool verbose = (MyPID==0);
   
	// generate Epetr_CrsMatrix pointer

	Epetra_CrsMatrix *A = NULL;
	err = ConstructMotionMatrix(A,m,n,nframes,nlayers,deltax,deltay,comm);
	if(verbose)
	cout << "Flag of constructing motion matrix is " << err << endl;
	//err = RowMatrixToMatrixMarketFile("Data/coefficient.mm",*A);

	// generate the pupil mask matrix (small ones for each frames)
	Epetra_CrsMatrix *W = NULL;
	err = ConstructDiagonalPupilMatrix(W,m,n,nframes,nlayers,deltax,deltay,vals,comm);
	if(verbose)
	cout << "Flag of construct the window matrix is " << err << endl;
	//err = RowMatrixToMatrixMarketFile("Data/mask.mm", *Wp);

	// generate the downsampling matrix  (small ones for each frames)
	Epetra_CrsMatrix *R = NULL;
	err = ConstructDiagonalDownSampleMatrix(R,m,n,nframes,nlayers,deltax,deltay,ssp,comm);
	if(verbose)
	cout << "Flag of construct the downsampling matrix is " << err << endl;

	Epetra_CrsMatrix *WpA = NULL;
	err = ConstructPupilMotionMatrix(WpA,m,n,nframes,nlayers,deltax,deltay,vals,comm);
	if(verbose)
		cout << "Flag of constructing window times motion matrix is " << err << endl;	
	
	Epetra_CrsMatrix *Dx = NULL;
	Epetra_CrsMatrix *Dy = NULL;
	// Make the Dx matrix, size is m+2 and n+2 since we will pad it, setting the scale to 1 for now.
	err = ConstructGradMatrix(Dx, 1, m+2, n+2, 1, comm);
	if(err) return err;

	err = ConstructGradMatrix(Dy, 2, m+2, n+2, 1, comm);
	if(err) return err;

	int const padSize = 1;
	Epetra_CrsMatrix *Pad = NULL;
	err = ConstructPadMatrix(Pad, m, n, padSize, comm);
	if(err) return err;

	Epetra_CrsMatrix *P = NULL;
	err = ConstructPhasePupilMatrix(P,m,n,vals,padSize,Pad, comm);
	if(err) return err;

	double reguParam = 2.22e-16; //hardcoded for now from one of the tests...	

	Epetra_CrsMatrix *PhaseRecon = NULL;
	err = ConstructPhaseReconMatrix(PhaseRecon, m, n, padSize, P, reguParam, comm);
	if(err) return err;

	// Declaration of vectors.
	Epetra_MultiVector *W_in = NULL;
	Epetra_MultiVector *R_in = NULL;
	Epetra_MultiVector *A_in = NULL;
	Epetra_MultiVector *DxDy_in = NULL;
	Epetra_MultiVector *Pad_in = NULL;
	Epetra_MultiVector *PhaseRecon_in = NULL;
	Epetra_MultiVector *PhasePupil_in = NULL;

	// define the map for the reading in data and load the data
	Epetra_Map W_domain(W->DomainMap()); 
	err = ReadMatAsMultiVector(file_W_in, comm, W_in, W_domain);
	if (verbose)
	cout << "read in data flag is " << err << endl;
	Epetra_Map R_domain(R->DomainMap()); 
	err = ReadMatAsMultiVector(file_R_in, comm, R_in, R_domain);
	if (verbose)
	cout << "read in data flag is " << err << endl;
	Epetra_Map A_domain(A->DomainMap()); 
	err = ReadMatAsMultiVector(file_A_in, comm, A_in, A_domain);
	if (verbose)
	cout << "read in data flag is " << err << endl;
	Epetra_Map DxDy_domain(Dx->DomainMap()); 
	err = ReadMatAsMultiVector(file_DxDy_in, comm, DxDy_in, DxDy_domain);
	if (verbose)
	cout << "read in data flag is " << err << endl;
	Epetra_Map Pad_domain(Pad->DomainMap()); 
	err = ReadMatAsMultiVector(file_Pad_in, comm, Pad_in, Pad_domain);
	if (verbose)
	cout << "read in data flag is " << err << endl;
	Epetra_Map PhaseRecon_domain(PhaseRecon->DomainMap()); 
	err = ReadMatAsMultiVector(file_PhaseRecon_in, comm, PhaseRecon_in, PhaseRecon_domain);
	if (verbose)
	cout << "read in data flag is " << err << endl;
	Epetra_Map PhasePupil_domain(P->DomainMap());
	err = ReadMatAsMultiVector(file_PhasePupil_in, comm, PhasePupil_in, PhasePupil_domain);
	if (verbose)
	cout << "read in data flag is " << err << endl;



	int blocksize = 1;
	// Setup some more problem/solver parameters:
	const Epetra_Map * W_range = &(W->RangeMap());
	Teuchos::RCP<Epetra_MultiVector> W_out = Teuchos::rcp( new Epetra_MultiVector(*W_range,blocksize) );

	const Epetra_Map * R_range = &(R->RangeMap());
	Teuchos::RCP<Epetra_MultiVector> R_out = Teuchos::rcp( new Epetra_MultiVector(*R_range,blocksize) );

	
	const Epetra_Map * A_range = &(A->RangeMap());
	Teuchos::RCP<Epetra_MultiVector> A_out = Teuchos::rcp( new Epetra_MultiVector(*A_range,blocksize) );

	const Epetra_Map * Dx_range = &(Dx->RangeMap());
	Teuchos::RCP<Epetra_MultiVector> Dx_out = Teuchos::rcp( new Epetra_MultiVector(*Dx_range,blocksize) );
	
	const Epetra_Map * Dy_range = &(Dy->RangeMap());
	Teuchos::RCP<Epetra_MultiVector> Dy_out = Teuchos::rcp( new Epetra_MultiVector(*Dy_range,blocksize) );

	const Epetra_Map * Pad_range = &(Pad->RangeMap());
	Teuchos::RCP<Epetra_MultiVector> Pad_out = Teuchos::rcp( new Epetra_MultiVector(*Pad_range,blocksize) );

	const Epetra_Map * PhaseRecon_range = &(PhaseRecon->RangeMap());
	Teuchos::RCP<Epetra_MultiVector> PhaseRecon_out = Teuchos::rcp( new Epetra_MultiVector(*PhaseRecon_range,blocksize) );

	const Epetra_Map * PhasePupil_range = &(P->RangeMap());
	Teuchos::RCP<Epetra_MultiVector> PhasePupil_out = Teuchos::rcp(new Epetra_MultiVector(*PhasePupil_range,blocksize) );



	//W->Multiply(false,*W_in, *W_out);
	R->Multiply(false,*R_in, *R_out);
	A->Multiply(false,*A_in, *A_out);
	WpA->Multiply(false,*A_in,*W_out);
	Dx->Multiply(false,*DxDy_in,*Dx_out);
	Dy->Multiply(false,*DxDy_in,*Dy_out);
	Pad->Multiply(false,*Pad_in,*Pad_out);
	PhaseRecon->Multiply(false,*PhaseRecon_in,*PhaseRecon_out);
	P->Multiply(false,*PhasePupil_in,*PhasePupil_out);

	//write the vectors out
	double **vecToWriteW = new double*[1];
	vecToWriteW[0] = new double[W_out->GlobalLength()]; 
	MultiVectorToArray(vecToWriteW, *W_out, false);
	if(W_out->Map().Comm().MyPID()==0){
		cout << "writing with processor " << W_out->Map().Comm().MyPID() << endl;
		WriteMatArray(file_W_out, file_W_out_var, vecToWriteW[0], W_out->GlobalLength());
	}

	double **vecToWriteR = new double*[1];
	vecToWriteR[0] = new double[R_out->GlobalLength()];
	MultiVectorToArray(vecToWriteR, *R_out, false);
	if(R_out->Map().Comm().MyPID()==0){
		cout << "writing with processor " << R_out->Map().Comm().MyPID() << endl;
		WriteMatArray(file_R_out, file_R_out_var, vecToWriteR[0], R_out->GlobalLength());
	}

	double **vecToWriteA = new double*[1];
	vecToWriteA[0] = new double[A_out->GlobalLength()];  
	MultiVectorToArray(vecToWriteA, *A_out, false);
	if(A_out->Map().Comm().MyPID()==0){
		cout << "writing with processor " << A_out->Map().Comm().MyPID() << endl;
		WriteMatArray(file_A_out, file_A_out_var, vecToWriteA[0], A_out->GlobalLength());
	}

	double **vecToWriteW_unmod = new double*[1];
	vecToWriteW_unmod[0] = new double[W_in->GlobalLength()];  
	MultiVectorToArray(vecToWriteW_unmod, *W_in, false);
	if(W_in->Map().Comm().MyPID()==0){
		cout << "writing with processor " << W_in->Map().Comm().MyPID() << endl;
		WriteMatArray(file_W_unmod, file_W_unmod_var, vecToWriteW_unmod[0], W_in->GlobalLength());
	}

	double **vecToWriteDx = new double*[1];
	vecToWriteDx[0] = new double[Dx_out->GlobalLength()];  
	MultiVectorToArray(vecToWriteDx, *Dx_out, false);
	if(Dx_out->Map().Comm().MyPID()==0){
		cout << "writing with processor " << Dx_out->Map().Comm().MyPID() << endl;
		WriteMatArray(file_Dx_out, file_Dx_out_var, vecToWriteDx[0], Dx_out->GlobalLength());
	}

	double **vecToWriteDy = new double*[1];
	vecToWriteDy[0] = new double[Dy_out->GlobalLength()];  
	MultiVectorToArray(vecToWriteDy, *Dy_out, false);
	if(Dy_out->Map().Comm().MyPID()==0){
		cout << "writing with processor " << Dy_out->Map().Comm().MyPID() << endl;
		WriteMatArray(file_Dy_out, file_Dy_out_var, vecToWriteDy[0], Dy_out->GlobalLength());
	}

	double **vecToWritePad = new double*[1];
	vecToWritePad[0] = new double[Pad_out->GlobalLength()];  
	MultiVectorToArray(vecToWritePad, *Pad_out, false);
	if(Pad_out->Map().Comm().MyPID()==0){
		cout << "writing with processor " << Pad_out->Map().Comm().MyPID() << endl;
		WriteMatArray(file_Pad_out, file_Pad_out_var, vecToWritePad[0], Pad_out->GlobalLength());
	}

	double **vecToWritePhaseRecon = new double*[1];
	vecToWritePhaseRecon[0] = new double[PhaseRecon_out->GlobalLength()];  
	MultiVectorToArray(vecToWritePhaseRecon, *PhaseRecon_out, false);
	if(PhaseRecon_out->Map().Comm().MyPID()==0){
		cout << "writing with processor " << PhaseRecon_out->Map().Comm().MyPID() << endl;
		WriteMatArray(file_PhaseRecon_out, file_PhaseRecon_out_var, vecToWritePhaseRecon[0], PhaseRecon_out->GlobalLength());
	}
	double **vecToWritePhasePupil = new double*[1];
	vecToWritePhasePupil[0] = new double[PhasePupil_out->GlobalLength()];  
	MultiVectorToArray(vecToWritePhasePupil, *PhasePupil_out, false);
	if(PhaseRecon_out->Map().Comm().MyPID()==0){
		cout << "writing with processor " << PhasePupil_out->Map().Comm().MyPID() << endl;
		WriteMatArray(file_PhasePupil_out, file_PhasePupil_out_var, vecToWritePhasePupil[0], PhasePupil_out->GlobalLength());
	}

	

	delete A;
	delete W;
	delete R;
	delete WpA;
	delete Dx;
	delete Dy;
	delete Pad;
	delete PhaseRecon;
	delete P;

	return(EXIT_SUCCESS);

}


// Tests a case in the UnitTest directory my computing the result and comparing
// it to the Matlab result. The tests require a special XML file that contains more fields.
// Specifically, the XML file must contain information about the Matlab "truth" results that
// we want to read in as well.
int FFMTest(Epetra_MpiComm &comm, std::ostream &out, int outputVerbosity, std::string params_file){

	int err;
	
	// Get our processor ID (0 if running serial)
	int MyPID = comm.MyPID();
	// Verbose flag: only processor 0 should print
	bool verbose = (MyPID==0);

	//read in the C++ gradient reconstruction and the Matlab reconstruction
	// get param list
	Teuchos::RCP< Teuchos::ParameterList > pl = Teuchos::getParametersFromXmlFile(params_file);
	int m = pl->get<int>("m");
	int n = pl->get<int>("n");
	int m_img = pl->get<int>("m_img");
	int n_img = pl->get<int>("n_img");
	int nframes = pl->get<int>("nframes");
	int nlayers = pl->get<int>("nlayers");
	const char* file_wind_vecs = (pl->get<std::string>("file_wind_vecs")).c_str();
	const char* file_pupil_mask = (pl->get<std::string>("file_pupil_mask")).c_str();
	
	int ssp = pl->get<int>("ssp");

	double deltax[nlayers];
	double deltay[nlayers];
	double *vals = new double[n*m];
	double wind_vecs[2*nlayers];

	// Read in the wind_vecs. This reads them in as a vector so the first nlayers entries will be the magnitude
	// and the second nlayers entries will be the angles.
	err = ReadMatAsStdVector(file_wind_vecs, wind_vecs, 2*nlayers);
	if (err)
			cout << "read in wind_vecs flag is " << err << endl;
	err = ConvertWindVecs(deltax, deltay, wind_vecs, nlayers, ssp); 
	if (err)
			cout << "convert to deltax/y flag is " << err << endl;
	err = ReadMatAsStdVector(file_pupil_mask, vals, m*n);
	if (err)
			cout << "read in pupil mask flag is " << err << endl;


	int mLarge, nLarge;
	err = GetCompositeImageSize(&mLarge, &nLarge, m, n, deltax, deltay, nframes, nlayers);

	///////////////////////////////////////////////////////////////////////////////////////
	//// COMPARE THE PHASEC (COMPOSITE) RESULTS
	///////////////////////////////////////////////////////////////////////////////////////

	const char* file_phaseC_gradx = (pl->get<std::string>("file_phaseC_gradx")).c_str();
	const char* file_phaseC_grady = (pl->get<std::string>("file_phaseC_grady")).c_str();
	const char* file_phaseC_gradx_matlab = (pl->get<std::string>("file_phaseC_gradx_matlab")).c_str();
	const char* file_phaseC_grady_matlab = (pl->get<std::string>("file_phaseC_grady_matlab")).c_str();
 

	Epetra_MultiVector *phaseC_gradx = NULL;
	Epetra_MultiVector *phaseC_grady = NULL;
	Epetra_MultiVector *phaseC_gradx_matlab = NULL;
	Epetra_MultiVector *phaseC_grady_matlab = NULL;

	// define the map for the reading in data and load the data
	int phaseCGradDim = mLarge*nLarge*nlayers;
	Epetra_Map phaseCGradMap(phaseCGradDim, 0, comm);
	
	// read in the matlab and C++ reconstructions as multivectors
	err = ReadMatAsMultiVector(file_phaseC_gradx, comm, phaseC_gradx, phaseCGradMap);
	if (verbose)
		cout << "read in phaseC_gradx flag is " << err << endl;

	err = ReadMatAsMultiVector(file_phaseC_grady, comm, phaseC_grady, phaseCGradMap);  
	if (verbose)
		cout << "read in phaseC_grady flag is " << err << endl;

	err = ReadMatAsMultiVector(file_phaseC_gradx_matlab, comm, phaseC_gradx_matlab, phaseCGradMap);
	if (verbose)
		cout << "read in phaseC_gradx_matlab flag is " << err << endl;

	err = ReadMatAsMultiVector(file_phaseC_grady_matlab, comm, phaseC_grady_matlab, phaseCGradMap);  
	if (verbose)
		cout << "read in phaseC_grady_matlab flag is " << err << endl;



	
	//compute norm(phaseC_gradx - phaseC_gradx_matlab)/norm(phaseC_gradx_matlab)
	double phaseC_gradx_RelResNorm;
	double phaseC_grady_RelResNorm;
	double numer;
	double denom;
	
	Epetra_MultiVector *phaseC_gradx_diff = new Epetra_MultiVector(phaseCGradMap,1);
	Epetra_MultiVector *phaseC_grady_diff = new Epetra_MultiVector(phaseCGradMap,1);

	phaseC_gradx_diff->Update(-1.0,*phaseC_gradx_matlab,1.0,*phaseC_gradx,1.0);
	phaseC_gradx_diff->Norm2(&numer);
	phaseC_gradx_matlab->Norm2(&denom);
	phaseC_gradx_RelResNorm = numer/denom;

	if(comm.MyPID() == 0){
		cout << "phaseC_gradx_RelNorm: " << phaseC_gradx_RelResNorm << endl;
	}


	phaseC_grady_diff->Update(-1.0,*phaseC_grady_matlab,1.0,*phaseC_grady,1.0);
	phaseC_grady_diff->Norm2(&numer);
	phaseC_grady_matlab->Norm2(&denom);
	phaseC_grady_RelResNorm = numer/denom;
	
	if(comm.MyPID() == 0){
		cout << "phaseC_grady_RelNorm: " << phaseC_grady_RelResNorm << endl;
	}


	delete phaseC_gradx;
	delete phaseC_grady;
	delete phaseC_gradx_matlab;
	delete phaseC_grady_matlab;
	delete phaseC_gradx_diff;
	delete phaseC_grady_diff;

	///////////////////////////////////////////////////////////////////////////
	//// COMPARE THE PHASE (PUNCHED OUT) RESULTS
	///////////////////////////////////////////////////////////////////////////
	
	const char* file_phase_gradx = (pl->get<std::string>("file_phase_gradx")).c_str();
	const char* file_phase_grady = (pl->get<std::string>("file_phase_grady")).c_str();
	const char* file_phase_gradx_matlab = (pl->get<std::string>("file_phase_gradx_matlab")).c_str();
	const char* file_phase_grady_matlab = (pl->get<std::string>("file_phase_grady_matlab")).c_str();

	Epetra_MultiVector *phase_gradx = NULL;
	Epetra_MultiVector *phase_grady = NULL;
	Epetra_MultiVector *phase_gradx_matlab = NULL;
	Epetra_MultiVector *phase_grady_matlab = NULL;

	// define the map for the reading in data and load the data
	int phaseGradDim = m*n*nframes;
	Epetra_Map phaseGradMap(phaseGradDim, 0, comm);
	
	// read in the matlab and C++ reconstructions as multivectors
	err = ReadMatAsMultiVector(file_phase_gradx, comm, phase_gradx, phaseGradMap);
	if (verbose)
		cout << "read in phase_gradx flag is " << err << endl;

	err = ReadMatAsMultiVector(file_phase_grady, comm, phase_grady, phaseGradMap);  
	if (verbose)
		cout << "read in phase_grady flag is " << err << endl;

	err = ReadMatAsMultiVector(file_phase_gradx_matlab, comm, phase_gradx_matlab, phaseGradMap);
	if (verbose)
		cout << "read in phase_gradx_matlab flag is " << err << endl;

	err = ReadMatAsMultiVector(file_phase_grady_matlab, comm, phase_grady_matlab, phaseGradMap);  
	if (verbose)
		cout << "read in phase_grady_matlab flag is " << err << endl;
	
	//compute norm(phaseC_gradx - phaseC_gradx_matlab)/norm(phaseC_gradx_matlab)
	double phase_gradx_RelResNorm;
	double phase_grady_RelResNorm;
	//double numer;
	//double denom;
	
	Epetra_MultiVector *phase_gradx_diff = new Epetra_MultiVector(phaseGradMap,1);
	Epetra_MultiVector *phase_grady_diff = new Epetra_MultiVector(phaseGradMap,1);

	phase_gradx_diff->Update(-1.0,*phase_gradx_matlab,1.0,*phase_gradx,1.0);
	phase_gradx_diff->Norm2(&numer);
	phase_gradx_matlab->Norm2(&denom);
	phase_gradx_RelResNorm = numer/denom;
	
	if(comm.MyPID()==0){
		cout << "phase_gradx_RelNorm: " << phase_gradx_RelResNorm << endl;
	}


	phase_grady_diff->Update(-1.0,*phase_grady_matlab,1.0,*phase_grady,1.0);
	phase_grady_diff->Norm2(&numer);
	phase_grady_matlab->Norm2(&denom);
	phase_grady_RelResNorm = numer/denom;

	if(comm.MyPID() == 0){
		cout << "phase_grady_RelNorm: " << phase_grady_RelResNorm << endl;
	}

	delete phase_gradx;
	delete phase_grady;
	delete phase_gradx_matlab;
	delete phase_grady_matlab;
	delete phase_gradx_diff;
	delete phase_grady_diff;

	/////////////////////////////////////////////////////////////////////
	//// COMPARE THE PHASE RESULTS
	/////////////////////////////////////////////////////////////////////
	
	// Just remember, we treat everything as a vector here.

	const char* file_phase = (pl->get<std::string>("file_phase")).c_str();
	const char* file_phase_matlab = (pl->get<std::string>("file_phase_matlab")).c_str();

	Epetra_Map phaseMap(m*n*nframes,0,comm);

	double phase_std[1][m*n*nframes];
	err = ReadMatAsStdVector(file_phase, phase_std[0], m*n*nframes);
	if (verbose)
		cout << "read in phase flag is " << err << endl;

	double** phase_matlab_std = new double*[1];
	phase_matlab_std[0] = new double[m*n*nframes];
	err = ReadMatAsStdVector(file_phase_matlab, phase_matlab_std[0], m*n*nframes);
	if (verbose)
		cout << "read in phase_matlab flag is " << err << endl;

	double** phase_diff_std = new double*[1];
	phase_diff_std[0] = new double[m*n*nframes];


	// Find the constants that null space differs by.
	double constants[nframes];
	for(int i=0;i<nframes;++i){
		constants[i] = phase_std[0][i*m*n+196] - phase_matlab_std[0][i*m*n+196]; // the 196 is a randomly selected nonzero pixel
										 // after the mask has been applied. Won't always
										 // work for a different pixel mask
	}

	// Find the difference between the two vectors less the nullspace constant
	for(int i=0;i<m*n*nframes;++i){
		phase_diff_std[0][i] =  phase_std[0][i] - phase_matlab_std[0][i] - (constants[i/(m*n)])*(vals[i-i/(m*n)*m*n]);
										// multiply so we don't subtract from 0 values
	}


	Teuchos::DataAccess c1 = Teuchos::Copy;
	Epetra_DataAccess c2 = static_cast<Epetra_DataAccess>(c1); // This is a hack to get around the fact that there
								   // are two enums called Copy, and only the Teuchos
								   // one has a class associated to it.
	
	Epetra_MultiVector *phase_diff = new Epetra_MultiVector(c2, phaseMap,phase_diff_std,1);
	Epetra_MultiVector *phase_matlab = new Epetra_MultiVector(c2, phaseMap,phase_matlab_std,1);

	double phase_RelResNorm;
	phase_diff->Norm2(&numer);
	phase_matlab->Norm2(&denom);
	phase_RelResNorm = numer/denom;

	if(comm.MyPID() == 0){
		cout << "phase_RelNorm: " << phase_RelResNorm << endl;
	}

	///////////////////////////////////////////////////////////////////////////////////////
	//// COMPARE THE RESTORED IMAGE RESULTS
	///////////////////////////////////////////////////////////////////////////////////////
	
	const char* file_restored = (pl->get<std::string>("file_restored")).c_str();
	const char* file_restored_matlab = (pl->get<std::string>("file_restored_matlab")).c_str();

	Epetra_Map restoreMap(m_img*n_img,0,comm);

	Epetra_MultiVector *restored = NULL;
	Epetra_MultiVector *restored_matlab = NULL;

		
	// read in the matlab and C++ nstructions as multivectors
	err = ReadMatAsMultiVector(file_restored, comm, restored, restoreMap);
	if (verbose)
		cout << "read in restored flag is " << err << endl;

	err = ReadMatAsMultiVector(file_restored_matlab, comm, restored_matlab, restoreMap);
	if (verbose)
		cout << "read in restored_matlab flag is " << err << endl;


	//compute norm(restored - restored_matlab)/norm(restored_matlab)
	double restored_RelResNorm;
	//double numer;
	//double denom;
	
	Epetra_MultiVector *restored_diff = new Epetra_MultiVector(restoreMap,1);

	restored_diff->Update(-1.0,*restored_matlab,1.0,*restored,1.0);
	restored_diff->Norm2(&numer);
	restored_matlab->Norm2(&denom);
	restored_RelResNorm = numer/denom;
	
	if(comm.MyPID()==0){
		cout << "restored_RelNorm: " << restored_RelResNorm << endl;
	}

	comm.Barrier();

	delete restored_diff;
	delete restored;
	delete restored_matlab;




	delete phase_diff;
	delete phase_matlab;
	delete[] phase_diff_std[0];
	delete[] phase_diff_std;
	delete[] phase_matlab_std[0];
	delete[] phase_matlab_std;

	delete[] vals;

	
	return(EXIT_SUCCESS);


}
