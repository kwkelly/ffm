int PunchOutFrames(Teuchos::RCP<Epetra_MultiVector> &phase, const Epetra_CrsMatrix *WpA, const Teuchos::RCP<Epetra_MultiVector> phaseC){
	/* This function takes the composite phase phaseC and punches out the frames and combines the layers. Thus the result phase
	 * is the phase in each frame separately.
	 * INPUTS
	 * 	- phase  - a reference to an Epetra_MultiVector. It should have length n*m*nframes 
	 * 	- WpA    - the matrix Wp*A which we use to compute the punched out phase
	 * 	- phaseC - the composite, multilayer phase as an Epetra_MultiVector. It should have length nLarge*mLarge*nlayers
	 * OUTPUTS
	 * 	- int    - Error code 0 is success, anything else is failure
	 */
	
	int err;
	err = WpA->Multiply(false,*phaseC,*phase);
	if(err) return err;

	return(EXIT_SUCCESS);
}

int ConstructGradxMatrixIndices(int *rowIdxLarge, int *colIdxLarge, double *valsLarge, int *rowIdx, int *colIdx, double *vals, const int m, const int n, const double scale){
	/* This function creates the indices necessary to make the GradxMatrix.
	 * INPUTS
	 * 	- rowIdxLarge - reference to an array of ints of length m*n
	 * 	- colIdxLarge - reference to an array of ints if length 4*m*n. This is because we are inputing 
	 * 			4 entries for each row. Some rows have fewer entries, but we give four values and 
	 * 			overlap the column indexes so they sum together to get what we need. Specifically,
	 * 			for row i, the column indices are those in colIdx[i] through colIdx[i+3]
	 * 	- valsLarge   - reference to an array of doubles of length 4*m*n. This will contain the values of the
	 * 			GradxMatrix associated with the columns and rows. Each entry in valsLarge corresponds
	 * 			to the same entry in colIdxLarge
	 * 	m 	      - row dimension of the matrices F and H
	 * 	n	      - col dimension of the matrices F and H
	 * 	scale	      - Scaling factor depending on specific geometries
	 * OUTPUTS
	 * 	int           - Error code 0 is success, anything else is failure
	 */


	for(int i=0;i<n-1;++i){
		rowIdx[i] = i;
		colIdx[2*i] = i;
		colIdx[2*i+1] = i+1;
		vals[2*i] = -scale*.5;
		vals[2*i+1] = scale*.5;

	}
	// last row is a different case
	rowIdx[n-1] = n-1;
	colIdx[2*n-2] = n-1;
	colIdx[2*n-1] = n-1;
	vals[2*n-2] = 0;
	vals[2*n-1] = 0;
	
	for(int j=0;j<n-1;++j){
		for(int i=0;i<n;++i){
			rowIdxLarge[j*n+i] = j*n+i;

			colIdxLarge[4*j*n+4*i]   = colIdx[2*i]   + j*m;
			colIdxLarge[4*j*n+4*i+1] = colIdx[2*i+1] + j*m;
			colIdxLarge[4*j*n+4*i+2] = colIdx[2*i]   + j*m + m;
			colIdxLarge[4*j*n+4*i+3] = colIdx[2*i+1] + j*m + m;

			valsLarge[4*j*n+4*i]   = vals[2*i];
			valsLarge[4*j*n+4*i+1] = vals[2*i+1];
			valsLarge[4*j*n+4*i+2] = vals[2*i];
			valsLarge[4*j*n+4*i+3] = vals[2*i+1];
			
		}
	}
	// Last block is slightly different. Instead of 2 H matrices next to each other in the Kronecker product, 
	// we just have 2H in the last row and last column
	int j=n-1;
	for(int i=0;i<n;++i){
		rowIdxLarge[j*n+i] = j*n+i;

		colIdxLarge[4*j*n+4*i]   = colIdx[2*i]   + j*m;
		colIdxLarge[4*j*n+4*i+1] = colIdx[2*i+1] + j*m;
		colIdxLarge[4*j*n+4*i+2] = colIdx[2*i]   + j*m;
		colIdxLarge[4*j*n+4*i+3] = colIdx[2*i+1] + j*m;


		valsLarge[4*j*n+4*i]   = vals[2*i];
		valsLarge[4*j*n+4*i+1] = vals[2*i+1];
		valsLarge[4*j*n+4*i+2] = vals[2*i];
		valsLarge[4*j*n+4*i+3] = vals[2*i+1];

	}



	return(EXIT_SUCCESS);
}

int ConstructGradyMatrixIndices(int *rowIdxLarge, int *colIdxLarge, double *valsLarge, int *rowIdx, int *colIdx, double *vals, const int m, const int n, const double scale){
	/* This function creates the indices necessary to make the GradyMatrix. Note the difference from above!!
	 * INPUTS
	 * 	- rowIdxLarge - reference to an array of ints of length m*n
	 * 	- colIdxLarge - reference to an array of ints if length 4*m*n. This is because we are inputing 
	 * 			4 entries for each row. Some rows have fewer entries, but we give four values and 
	 * 			overlap the column indexes so they sum together to get what we need. Specifically,
	 * 			for row i, the column indices are those in colIdx[i] through colIdx[i+3]
	 * 	- valsLarge   - reference to an array of doubles of length 4*m*n. This will contain the values of the
	 * 			GradxMatrix associated with the columns and rows. Each entry in valsLarge corresponds
	 * 			to the same entry in colIdxLarge
	 * 	m 	      - row dimension of the matrices F and H
	 * 	n	      - col dimension of the matrices F and H
	 * 	scale	      - Scaling factor depending on specific geometries
	 * OUTPUTS
	 * 	int           - Error code 0 is success, anything else is failure
	 */


	// Generate indices for F
	for(int i=0;i<n-1;++i){
		rowIdx[i] = i;
		colIdx[2*i] = i;
		colIdx[2*i+1] = i+1;
		vals[2*i] = scale*.5;
		vals[2*i+1] = scale*.5;
	}
	// last row is a different case
	rowIdx[n-1] = n-1;
	colIdx[2*n-2] = n-1;
	colIdx[2*n-1] = n-1;
	vals[2*n-2] = scale*.5;
	vals[2*n-1] = scale*.5;

	for(int j=0;j<n-1;++j){
		for(int i=0;i<n;++i){
			rowIdxLarge[j*n+i] = j*n+1;

			colIdxLarge[4*j*n+4*i]   = colIdx[2*i]   + j*m;
			colIdxLarge[4*j*n+4*i+1] = colIdx[2*i+1] + j*m;
			colIdxLarge[4*j*n+4*i+2] = colIdx[2*i]   + j*m + m;
			colIdxLarge[4*j*n+4*i+3] = colIdx[2*i+1] + j*m + m;

			valsLarge[4*j*n+4*i]   = -vals[2*i];
			valsLarge[4*j*n+4*i+1] = -vals[2*i+1];
			valsLarge[4*j*n+4*i+2] =  vals[2*i];
			valsLarge[4*j*n+4*i+3] =  vals[2*i+1];
		}
	}
	// Last block is 0
	int j=n-1;
	for(int i=0;i<n;++i){
		rowIdxLarge[j*n+i] = j*n+1;

		colIdxLarge[4*j*n+4*i]   = colIdx[2*i]   + j*m;
		colIdxLarge[4*j*n+4*i+1] = colIdx[2*i+1] + j*m;
		colIdxLarge[4*j*n+4*i+2] = colIdx[2*i]   + j*m;
		colIdxLarge[4*j*n+4*i+3] = colIdx[2*i+1] + j*m;

		valsLarge[4*j*n+4*i]   = 0;
		valsLarge[4*j*n+4*i+1] = 0;
		valsLarge[4*j*n+4*i+2] = 0;
		valsLarge[4*j*n+4*i+3] = 0;
	}
	
	return(EXIT_SUCCESS);
}

/*
int ConstructPhaseReconMatrixIndices(int *rowIdxLarge, int *colIdxLarge, double *valsLarge, int *rowIdxLargeDx, int *colIdxLargeDx, double *valsLargeDx, int *rowIdxLargeDy, int *colIdxLargeDy, double *valsLargeDy, const int m, const int n){
	
	 Convert the indices from the Dx and Dy matrices into indices for Dx stacked on top of Dy
	 

	// Collect information from the Dx matrix into the first n*n rows
	for(int i=0;i<n*n-1;++i){
		rowIdxLarge[i] = rowIdxLargeDx[i];
		
		colIdxLarge[4*i]   = colIdxLargeDx[4*i];
		colIdxLarge[4*i+1] = colIdxLargeDx[4*i+1];
		colIdxLarge[4*i+2] = colIdxLargeDx[4*i+2];
		colIdxLarge[4*i+3] = colIdxLargeDx[4*i+3];

		valsLarge[4*i]     = valsLargeDx[4*i];
		valsLarge[4*i+1]   = valsLargeDx[4*i+1];
		valsLarge[4*i+2]   = valsLargeDx[4*i+2];
		valsLarge[4*i+3]   = valsLargeDx[4*i+3];
	}

	// Dy matrix entries go into the following n*n rows
	for(int i=0;i<n*n-1;++i){
		
		int j = n*n + i;

		rowIdxLarge[j] = rowIdxLargeDy[i] + n*n;
		
		colIdxLarge[4*j]   = colIdxLargeDy[4*i];
		colIdxLarge[4*j+1] = colIdxLargeDy[4*i+1];
		colIdxLarge[4*j+2] = colIdxLargeDy[4*i+2];
		colIdxLarge[4*j+3] = colIdxLargeDy[4*i+3];

		valsLarge[4*j]     = valsLargeDy[4*i];
		valsLarge[4*j+1]   = valsLargeDy[4*i+1];
		valsLarge[4*j+2]   = valsLargeDy[4*i+2];
		valsLarge[4*j+3]   = valsLargeDy[4*i+3];
	

	return(EXIT_SUCCESS);
}



int ConstructPhaseReconMatrix(Epetra_CrsMatrix *&PhaseMat, const int m, const int n, const double scale, const double *pupil_mask, Epetra_MpiComm &comm){
* This fucntion creates the GradxMatrix for phase reconstruction.
	 * INPUTS
	 * 	- PhaseMat   - Reference to a pointer to an Epetra_CrsMatrix to store the matrix
	 * 	- m          - row dimension of the matrices F and H
	 * 	- n          - col dimension of the matrices F and H
	 * 	- scale      - scaling factor depending on geormetry
	 * 	- pupil_mask - array of doubles containing the values in the pupil_mask as a vector.
	 * 	- comm  - the MPI communicator that we are using for communication
	 * OUTPUTS
	 * 	- int   - Error code. 0 for success, anything else for failure
	 *
	
	int err;

	int* rowIdxLargeDx = new int[m*n];
	int* colIdxLargeDx = new int[4*m*n]; // see documentation for ConstructGradxMatrixIndices for size difference explanation
	double* valsLargeDx = new double[4*m*n];

	int* rowIdxLargeDy = new int[m*n];
	int* colIdxLargeDy = new int[4*m*n];
	double* valsLargeDy = new double[4*m*n];

	// Get indices for Dx and Dy matrices
	err = ConstructGradxMatrixIndices(rowIdxLargeDx, colIdxLargeDx, valsLargeDx, m, n, scale);
	if(err) return err;

	err = ConstructGradyMatrixIndices(rowIdxLargeDy, colIdxLargeDy, valsLargeDy, m, n, scale);
	if(err) return err;

	int rowIdxLarge[2*m*n];
	int colIdxLarge[8*m*n];
	double valsLarge[8*m*n];

	// Get indices of the two stacked on top of each other.
	err = ConstructPhaseReconMatrixIndices(rowIdxLarge,colIdxLarge,valsLarge, rowIdxLargeDx, colIdxLargeDx, valsLargeDx, rowIdxLargeDy, colIdxLargeDy, valsLargeDy, m, n);
	if(err) return err;

	// define Epetra_CrsMatrix PhaseMat: row and col dimensions
	int rowDimensions = 2*m*n;
	int colDimensions = m*n;

	// define Epetra_CrsMatrix R: row and col map
	Epetra_Map rowMap(rowDimensions, 0, comm);
	Epetra_Map colMap(colDimensions, 0, comm);

	// define Epetra_CrsMatrix PhaseMat: row local and global dimensions
	int myRowDimensions = rowMap.NumMyElements();
	int *myRowGlobalDimensions = rowMap.MyGlobalElements();

	// define number of entries per row
	int numEntries = 4;

	//PhaseMat = new Epetra_CrsMatrix(Copy, rowMap,colMap, numEntries);
	PhaseMat = new Epetra_CrsMatrix(Copy, rowMap, numEntries);

	// fill in the matrix data
	for(int index=0; index<myRowDimensions; ++index) {
		int globalRowIdx = myRowGlobalDimensions[index];

		// Only fill in a row if the corresponding row of the pupil_mask is nonempty. Otherwise, it is blank
		if(pupil_mask[globalRowIdx % (m*n)] != 0){
			(*PhaseMat).InsertGlobalValues(globalRowIdx,numEntries,&valsLarge[globalRowIdx*numEntries],&rowIdxLarge[globalRowIdx*numEntries]);
		}
	}

	(*PhaseMat).FillComplete(colMap,rowMap);
	
	
	return(EXIT_SUCCESS);

}

*/

int ConstructGradMatrix(Epetra_CrsMatrix *&GradMat, const int mat, const int m, const int n, const double scale, Epetra_MpiComm &comm){

	/* This fucntion creates the GradxMatrix and GradyMatrix for phase reconstruction.
	 * INPUTS
	 * 	- GradMat    - Reference to a pointer to an Epetra_CrsMatrix to store the matrix
	 * 	- mat	     - Integer specifying if we are making the Gradx or Grady matrix. 1 for x, 2 for y
	 * 	- m          - row dimension of the matrices F and H
	 * 	- n          - col dimension of the matrices F and H
	 * 	- scale      - scaling factor depending on geormetry
	 * 	- comm  - the MPI communicator that we are using for communication
	 * OUTPUTS
	 * 	- int   - Error code. 0 for success, anything else for failure
	 */
	
	int err;

	int *rowIdx = new int[n];
	int *colIdx = new int[2*n];
	double *vals = new double[2*n];	

	int* rowIdxLarge = new int[m*n];
	int* colIdxLarge = new int[4*m*n]; // see documentation for ConstructGradxMatrixIndices for size difference explanation
	double* valsLarge = new double[4*m*n];

	if(mat == 1){
		err = ConstructGradxMatrixIndices(rowIdxLarge, colIdxLarge, valsLarge, rowIdx, colIdx, vals, m, n, scale);
		if(err) return err;
	}
	else if(mat == 2){
		err = ConstructGradyMatrixIndices(rowIdxLarge, colIdxLarge, valsLarge, rowIdx, colIdx, vals, m, n, scale);
		if(err) return err;
	}

	// define Epetra_CrsMatrix GradxMat row and col dimensions
	int rowDimensions = m*n;
	int colDimensions = m*n;

	// define Epetra_CrsMatrix R: row and col map
	Epetra_Map rowMap(rowDimensions, 0, comm);
	Epetra_Map colMap(colDimensions, 0, comm);

	// define Epetra_CrsMatrix PhaseMat: row local and global dimensions
	int myRowDimensions = rowMap.NumMyElements();
	int *myRowGlobalDimensions = rowMap.MyGlobalElements();

	// define number of entries per row
	int numEntries = 4;

	//PhaseMat = new Epetra_CrsMatrix(Copy, rowMap,colMap, numEntries);
	GradMat = new Epetra_CrsMatrix(Copy, rowMap, numEntries);

	// fill in the matrix data
	for(int index=0; index<myRowDimensions; ++index) {
		int globalRowIdx = myRowGlobalDimensions[index];
		(*GradMat).InsertGlobalValues(globalRowIdx,numEntries,&valsLarge[globalRowIdx*numEntries],&colIdxLarge[globalRowIdx*numEntries]);
		
	}

	(*GradMat).FillComplete(colMap,rowMap);

	delete[] rowIdx;
	delete[] colIdx;
	delete[] vals;
	delete[] rowIdxLarge;
	delete[] colIdxLarge;
	delete[] valsLarge;
	
	return(EXIT_SUCCESS);
}


int ConstructRegularizationMatrix(Epetra_CrsMatrix *&ReguMat, const int m, const int n, const double reguParam, Epetra_MpiComm &comm){

	/* This fucntion creates the regularization matrix for the normal equations for thephase reconstruction problem 
	 * INPUTS
	 * 	- ReguMat   - Reference to a pointer to an Epetra_CrsMatrix to store the matrix
	 * 	- m         - row dimension of the matrix
	 * 	- n         - col dimension of the matrix (should be the same as the row dimension since we have a square matrix)
	 * 	- reguParam - The value of the reugularization parameter
	 * 	- comm      - the MPI communicator that we are using for communication
	 * OUTPUTS
	 * 	- int   - Error code. 0 for success, anything else for failure
	 */

	// define Epetra_CrsMatrix ReguMat row and col dimensions
	int rowDimensions = m*m;
	int colDimensions = m*m; //gotta be square

	// define Epetra_CrsMatrix PadMat row and col map
	Epetra_Map rowMap(rowDimensions, 0, comm);
	Epetra_Map colMap(colDimensions, 0, comm);

	int numEntries = 1;

	ReguMat = new Epetra_CrsMatrix(Copy, rowMap, numEntries);

	int myRowDimensions = rowMap.NumMyElements();
	int *myRowGlobalDimensions = rowMap.MyGlobalElements();

	for(int index=0; index<myRowDimensions; ++index) {
		int globalRowIdx = myRowGlobalDimensions[index];
		//cout << "globalRowIdx: " << globalRowIdx << endl;
		(*ReguMat).InsertGlobalValues(globalRowIdx,numEntries,&reguParam,&globalRowIdx);
		
	}

	(*ReguMat).FillComplete(colMap,rowMap);

	return(EXIT_SUCCESS);

}

int ConstructPadMatrix(Epetra_CrsMatrix *&PadMat, const int m, const int n, const int padSize, Epetra_MpiComm &comm){

	/* This fucntion creates the Pad Matrix for padding an array with zeros around the border. Specifically, if an array 
	 * is m-by-n, then this function creates a matrix that pads the vector associated with that array. The resulting 
	 * vector produced after multiplication with the PadMat should have size ((m+2*padSize)*(n+2*padSize))-by-(m*n). The
	 * matrix comes from the kronecker product of matrices that pad top/bottom and left/right sides of the matrix via
	 * multiplication on the left and right, respectively.
	 * INPUTS
	 * 	- PadMat    - Reference to a pointer to an Epetra_CrsMatrix to store the matrix
	 * 	- m         - row dimension of the matrix to pad
	 * 	- n         - col dimension of the matrix to pad
	 * 	- padSize   - Number of pixels to pad around the border
	 * 	- comm      - the MPI communicator that we are using for communication
	 * OUTPUTS
	 * 	- int   - Error code. 0 for success, anything else for failure
	 */
		

	int *rowIdx = new int[m*n];
	int *colIdx = new int[m*n];
	//Create the indices
	for(int j=0;j<n;++j){
		for(int i=0;i<m;++i){
			colIdx[j*m+i] = j*m+i;
			rowIdx[j*m+i] = (j+1)*(m+2*padSize) + padSize + i;
		}
	}

	// define Epetra_CrsMatrix PadMat row and col dimensions
	int rowDimensions = (m+2*padSize)*(n+2*padSize);
	int colDimensions = m*n;

	// define Epetra_CrsMatrix PadMat row and col map
	Epetra_Map rowMap(rowDimensions, 0, comm);
	Epetra_Map colMap(colDimensions, 0, comm);

	int numEntries = 1;
	double one = 1;

	PadMat = new Epetra_CrsMatrix(Copy, rowMap, numEntries);
	
	// Fill the matrix
	for(int i=0;i<m*n;++i){
		(*PadMat).InsertGlobalValues(rowIdx[i],numEntries,&one,&colIdx[i]);
	}


	(*PadMat).FillComplete(colMap,rowMap);

	delete[] rowIdx;
	delete[] colIdx;

	return(EXIT_SUCCESS);

}

int Reshape2DArray(double **arrayIn, int mIn, int nIn, double **arrayOut, int mOut, int nOut){
	/* Given a 2D standard C++ array of size arrayInM-by-arrayInN, this function will return an array
	 * reshaped to the size arrayOutM-by-arrayOutN. The total size of the array must not change. This is
	 * not done in place and so memory usage will double.
	 * INPUTS
	 * 	- arrayIn   - 2D std C++ array
	 * 	- mIn       - number of rows of the array going in
	 * 	- nIn       - number of columns of the array going in
	 * 	- arrayOut  - 2D std C++ array
	 * 	- mOut 	    - number of rows of the array going out
	 * 	- nOut      - number of columns of the array going out
	 * OUTPUTS
	 * 	- int	    - Error code. 0 for success, 1 for anything else.
	 */

	if(mIn*nIn != mOut*nOut){
		cout << "Arrays do not have the same total size" << endl;
		return -1;
	}


	int rowIn, colIn, rowOut, colOut;
	// Loop through all elements, and place the correct stuff everywhere
	for(int i=0;i<mIn*nIn;++i){
		rowIn = i-mIn*(i/mIn);
		colIn = i/mIn;
		rowOut = i-mOut*(i/mOut);
		colOut = i/mOut; //exploiting integer division for indexing

		arrayOut[colOut][rowOut] = arrayIn[colIn][rowIn];
	}

	return(EXIT_SUCCESS);


}



int Reshape2DArray(double *arrayIn, int mIn, int nIn, double **arrayOut, int mOut, int nOut){
	// Overloaded form of the above function with a 1 dim array as input
	/* Given a 1D standard C++ array of size mIn-by-nIn, this function will return an array
	 * reshaped to the size mOut-by-nOut. The total size of the array must not change. This is
	 * not done in place and so memory usage will double.
	 * INPUTS
	 * 	- arrayIn   - 2D std C++ array
	 * 	- mIn       - number of rows of the array going in
	 * 	- nIn       - number of columns of the array going in
	 * 	- arrayOut  - 2D std C++ array
	 * 	- mOut 	    - number of rows of the array going out
	 * 	- nOut      - number of columns of the array going out
	 * OUTPUTS
	 * 	- int	    - Error code. 0 for success, 1 for anything else.
	 */

	if(mIn*nIn != mOut*nOut){
		cout << "Arrays do not have the same total size" << endl;
		return -1;
	}


	int rowIn, colIn, rowOut, colOut;
	// Loop through all elements, and place the correct stuff everywhere
	for(int i=0;i<mIn*nIn;++i){
		rowIn = i-mIn*(i/mIn);
		colIn = i/mIn;
		rowOut = i-mOut*(i/mOut);
		colOut = i/mOut; //exploiting integer division for indexing

		arrayOut[colOut][rowOut] = arrayIn[rowIn];
	}

	return(EXIT_SUCCESS);
}


int ReshapeMultiVector(Epetra_MultiVector *&reshapedMultiVec, const int length, const int numVecs, const Epetra_MultiVector &originalMultiVec, Epetra_MpiComm &comm){

	/* Function to reshape a multivector. Does it by copying the values, so additional memory will be required.
	 * Note that this function does NOT delete the original vector.
	 * INPUTS
	 * 	reshapedMultiVec - This is a reference to a null pointer that will point to the
	 * 			   new (reshaped) multivector.
	 * 	length 		 - Global length of the new multivector
	 * 	numVecs		 - The number of vectors in the new multivector.
	 * 	originalMultiVec - The original multivector that is being reshaped.
	 * 	comm		 - MPI communicator
	 * OUTPUT
	 * 	int		 - Error flag
	 */

	int err;

	int numVecsOrig = originalMultiVec.NumVectors();
	int lengthOrig = originalMultiVec.GlobalLength();
	
	//extract phase_gradx and phase_grady as standard arrays so that we can reshape them.
	double** originalMultiVec_std = new double*[numVecsOrig];
	for(int i=0;i<numVecsOrig;++i){
		originalMultiVec_std[i] = new double[lengthOrig];
	}
	err = MultiVectorToArray(originalMultiVec_std, originalMultiVec, false);
	if(err) return err;

	// Copy the values to all the processors after extracting them.
	for(int i=0;i<numVecsOrig;++i){
		comm.Broadcast(originalMultiVec_std[i], lengthOrig, 0);
	}


	double** reshaped_std = new double*[numVecs];
	for(int i=0;i<numVecs;++i){
		reshaped_std[i] = new double[length];
	}
	err = Reshape2DArray(originalMultiVec_std, lengthOrig, numVecsOrig, reshaped_std, length, numVecs);
	if(err) return err;


	Epetra_Map reshapedMap(length,0,comm);

	//Initialize the vector
	int numMyElements = reshapedMap.NumMyElements();
	int* myGlobalElements = reshapedMap.MyGlobalElements(); 
	

	reshapedMultiVec = new Epetra_MultiVector(reshapedMap,numVecs);
	double ** Ap = reshapedMultiVec->Pointers();

	for (int j=0; j<numVecs; ++j)
	{
		double * v = Ap[j];

		// Fill it
		for (int i=0; i<numMyElements; ++i)
		{
			v[i] = reshaped_std[j][myGlobalElements[i]];
		}
	}

	
	// free memory
	for(int i=0;i<numVecsOrig;++i){
		delete[] originalMultiVec_std[i];
	}
	delete[] originalMultiVec_std;
	for(int i=0;i<numVecs;++i){
		delete[] reshaped_std[i];
	}
	delete[] reshaped_std;


	return(EXIT_SUCCESS);


}

int ConstructPhasePupilMatrix(Epetra_CrsMatrix *&P, const int m, const int n, double* pupil_vals, const int padSize, Epetra_CrsMatrix *&PadMat, Epetra_MpiComm &comm){

	/* This fucntion creates the Pupil matrix as it is used in the phase reconstruction problem. It is
	 * a square, diagonal matrix with the values of the pupil mask along the diagonal.
	 * INPUTS
	 * 	- P    	    - Reference to a pointer to an Epetra_CrsMatrix to store the matrix
	 * 	- m         - row dimension of the matrix
	 * 	- n         - col dimension of the matrix
	 * 	- comm      - the MPI communicator that we are using for communication
	 * OUTPUTS
	 * 	- int       - Error code. 0 for success, anything else for failure
	 */	

	Teuchos::DataAccess c1 = Teuchos::Copy;
	Epetra_DataAccess c2 = static_cast<Epetra_DataAccess>(c1); // This is a hack to get around the fact that there
								   // are two enums called Copy, and only the Teuchos
								   // one has a class associated to it.


	Epetra_Map unpaddedMap(m*n, 0, comm);
	Epetra_Map paddedMap((m+2*padSize)*(n+2*padSize),0,comm);
	Epetra_Vector *pupil_vals_vec = new Epetra_Vector(unpaddedMap);

	//Initialize the vector
	//
	//I'm inputting the data into the multivector in this way, because when I pass
	// pupil_vals to the Epetra_MultiVector constructor we get attempted access of 
	// uninitialized values. I think I need to use the constructor for an array instead
	// of pointers... Just now realized this
	// TODO - Try passing the constructor with MyLDA known
	int numMyElements = unpaddedMap.NumMyElements();
	int* myGlobalElements = unpaddedMap.MyGlobalElements(); 

	double ** Ap = pupil_vals_vec->Pointers();

	double * v = Ap[0];

	// Fill it
	for (int i=0; i<numMyElements; ++i)
	{
		v[i] = pupil_vals[myGlobalElements[i]];
	}	

	// The above properly fills the vector

	Epetra_Vector *pupil_vals_padded_vec = new Epetra_Vector(paddedMap);
	PadMat->Multiply(false,*pupil_vals_vec,*pupil_vals_padded_vec);

	double** pupil_vals_padded = new double*[1];
	pupil_vals_padded[0] = new double[pupil_vals_padded_vec->GlobalLength()];
	MultiVectorToArray(pupil_vals_padded, *pupil_vals_padded_vec, false);

	// Broadcast the pupil_vals to all the processors so that they all have access to them
	comm.Broadcast(pupil_vals_padded[0],pupil_vals_padded_vec->GlobalLength(),0);

	delete pupil_vals_vec;
	delete pupil_vals_padded_vec;

	// define Epetra_CrsMatrix GradxMat row and col dimensions
	int rowDimensions = (m+2*padSize)*(n+2*padSize);
	int colDimensions = (m+2*padSize)*(n+2*padSize);

	// define Epetra_CrsMatrix R: row and col map
	Epetra_Map rowMap(rowDimensions, 0, comm);
	Epetra_Map colMap(colDimensions, 0, comm);

	// define Epetra_CrsMatrix PhaseMat: row local and global dimensions
	int myRowDimensions = rowMap.NumMyElements();
	int *myRowGlobalDimensions = rowMap.MyGlobalElements();

	// define number of entries per row
	int numEntries = 1;

	//PhaseMat = new Epetra_CrsMatrix(Copy, rowMap,colMap, numEntries);
	P = new Epetra_CrsMatrix(Copy, rowMap, numEntries);

	// fill in the matrix data
	for(int index=0; index<myRowDimensions; ++index) {
		int globalRowIdx = myRowGlobalDimensions[index];
		(*P).InsertGlobalValues(globalRowIdx,numEntries,&pupil_vals_padded[0][globalRowIdx],&globalRowIdx);
	}
	
	(*P).FillComplete(colMap,rowMap);

	delete[] pupil_vals_padded[0];
	delete[] pupil_vals_padded;

	return(EXIT_SUCCESS);
}

int ConstructPhaseReconMatrix(Epetra_CrsMatrix *&PhaseReconMat, const int m, const int n, const int padSize, Epetra_CrsMatrix *&P, const double reguParam, Epetra_MpiComm &comm){

	Epetra_CrsMatrix *aI = NULL;
	// Construct the Gradx and Grady Matrices...
	Epetra_CrsMatrix *Dx = NULL;
	Epetra_CrsMatrix *Dy = NULL;

	int err;

	Teuchos::DataAccess c1 = Teuchos::Copy;
	Epetra_DataAccess c2 = static_cast<Epetra_DataAccess>(c1);


	// Make the Dx matrix, size is m+2 and n+2 since we will pad it, setting the scale to 1 for now.
	err = ConstructGradMatrix(Dx, 1, m+2*padSize, n+2*padSize, 1, comm);
	if(err) return err;

	err = ConstructGradMatrix(Dy, 2, m+2*padSize, n+2*padSize, 1, comm);
	if(err) return err;


	// With P included
	// Get PDx and PDy
	Epetra_CrsMatrix *PDx = new Epetra_CrsMatrix(c2, P->RowMap(), 4);
	err = MatrixMatrix::Multiply(*P,false,*Dx,false,*PDx);
	if(err) return err;

	Epetra_CrsMatrix *PDy = new Epetra_CrsMatrix(c2, P->RowMap(), 4);
	err = MatrixMatrix::Multiply(*P,false,*Dy,false,*PDy);
	if(err) return err;

	// Get DxTPDx and DyTPDy
	Epetra_CrsMatrix *DxTPDx = new Epetra_CrsMatrix(c2, Dx->RangeMap(), 6);
	err = MatrixMatrix::Multiply(*Dx,true,*PDx,false,*DxTPDx);
	if(err) return err;

	Epetra_CrsMatrix *DyTPDy = new Epetra_CrsMatrix(c2, Dy->RangeMap(), 6);
	err = MatrixMatrix::Multiply(*Dy,true,*PDy,false,*DyTPDy);
	if(err) return err;


	delete PDx;
	delete PDy;
	delete Dx;
	delete Dy;

	err = ConstructRegularizationMatrix(aI,m+2*padSize,n+2*padSize,reguParam,comm);
	if(err) return err;

	// Add 'em up! Note that this stores the sum in aI because of how the sum function works.
	Epetra_CrsMatrix *temp = NULL;
	MatrixMatrix::Add(*DyTPDy,false,1,*aI,false,1,temp);
	temp->FillComplete();
	MatrixMatrix::Add(*DxTPDx,false,1,*temp,false,1,PhaseReconMat);
	PhaseReconMat->FillComplete();
	
	delete temp;
	delete DyTPDy;
	delete DxTPDx;
	delete aI;


	// Without P included
/*	// Get DxTDx and DyTDy
	Epetra_CrsMatrix *DxTDx = new Epetra_CrsMatrix(c2, Dx->RangeMap(), 6);
	err = MatrixMatrix::Multiply(*Dx,true,*Dx,false,*DxTDx);
	if(err) return err;

	Epetra_CrsMatrix *DyTDy = new Epetra_CrsMatrix(c2, Dy->RangeMap(), 6);
	err = MatrixMatrix::Multiply(*Dy,true,*Dy,false,*DyTDy);
	if(err) return err;

	delete Dx;
	delete Dy;

	err = ConstructRegularizationMatrix(aI,m+2*padSize,n+2*padSize,reguParam,comm);
	if(err) return err;

	// Add 'em up! Note that this stores the sum in aI because of how the sum function works.
	Epetra_CrsMatrix *temp = NULL;
	MatrixMatrix::Add(*DyTDy,false,1,*aI,false,1,temp);
	temp->FillComplete();
	MatrixMatrix::Add(*DxTDx,false,1,*temp,false,1,PhaseReconMat);
	PhaseReconMat->FillComplete();



	delete temp;
	delete DyTDy;
	delete DxTDx;
	delete aI;

*/
	return(EXIT_SUCCESS);
}


int ConstructPhaseRHS(Epetra_MultiVector *&PhaseRHS, const int m, const int n, const int padSize, const int nframes, Teuchos::RCP<Epetra_MultiVector> phase_gradx, Teuchos::RCP<Epetra_MultiVector> phase_grady, Epetra_CrsMatrix *&PadMat, Epetra_CrsMatrix *&P, Epetra_MpiComm &comm){

	int err;
	
	// Reshape the multivector
	Epetra_MultiVector *phase_gradx_reshaped = NULL;	
	err = ReshapeMultiVector(phase_gradx_reshaped, m*n, nframes, *phase_gradx, comm);

	Epetra_MultiVector *phase_grady_reshaped = NULL;	
	err = ReshapeMultiVector(phase_grady_reshaped, m*n, nframes, *phase_grady, comm);

	// Now pad around each frame using the matrix method.
	Epetra_Map phase_grad_map_padded(((m+2*padSize)*(n+2*padSize)),0,comm);
	Epetra_MultiVector *phase_gradx_padded = new Epetra_MultiVector(phase_grad_map_padded,nframes);
	Epetra_MultiVector *phase_grady_padded = new Epetra_MultiVector(phase_grad_map_padded,nframes);

	// Pad the multivectors
	err = PadMat->Multiply(false,*phase_gradx_reshaped,*phase_gradx_padded);
	if(err) return err;
	
	err = PadMat->Multiply(false,*phase_grady_reshaped,*phase_grady_padded);
	if(err) return err;

	// Construct the Gradx and Grady Matrices...
	Epetra_CrsMatrix *Dx = NULL;
	Epetra_CrsMatrix *Dy = NULL;

	err = ConstructGradMatrix(Dx, 1, m+2*padSize, n+2*padSize, 1, comm); // Size is n+2*padSize because padding
	if(err) return err;

	err = ConstructGradMatrix(Dy, 2, m+2*padSize, n+2*padSize, 1, comm);
	if(err) return err;


	// Create the Right hand side of the inverse problem. No P in this case.
/*
	Epetra_MultiVector *DxTphase_gradx = new Epetra_MultiVector(Dx->DomainMap(),nframes);
	Epetra_MultiVector *DyTphase_grady = new Epetra_MultiVector(Dy->DomainMap(),nframes);
	err = Dx->Multiply(true,*phase_gradx_padded,*DxTphase_gradx);
	if(err) return err;
	err = Dy->Multiply(true,*phase_grady_padded,*DyTphase_grady);
	if(err) return err;


	PhaseRHS = new Epetra_MultiVector(Dx->DomainMap(),nframes);
	PhaseRHS->PutScalar( 0.0 );
	err = PhaseRHS->Update(1,*DxTphase_gradx,1,*DyTphase_grady,1);
*/
	// Create the Right hand side of the inverse problem with P included
	

	Epetra_MultiVector *Pphase_gradx = new Epetra_MultiVector(P->RangeMap(),nframes);
	Epetra_MultiVector *Pphase_grady = new Epetra_MultiVector(P->RangeMap(),nframes);
	err = P->Multiply(false,*phase_gradx_padded,*Pphase_gradx);
	if(err) return err;
	err = P->Multiply(false,*phase_grady_padded,*Pphase_grady);
	if(err) return err;

	Epetra_MultiVector *DxTPphase_gradx = new Epetra_MultiVector(Dx->DomainMap(),nframes);
	Epetra_MultiVector *DyTPphase_grady = new Epetra_MultiVector(Dy->DomainMap(),nframes);
	err = Dx->Multiply(true,*Pphase_gradx,*DxTPphase_gradx);
	if(err) return err;
	err = Dy->Multiply(true,*Pphase_grady,*DyTPphase_grady);
	if(err) return err;

	delete Pphase_gradx;
	delete Pphase_grady;

	PhaseRHS = new Epetra_MultiVector(Dx->DomainMap(),nframes);
	PhaseRHS->PutScalar( 0.0 );
	err = PhaseRHS->Update(1,*DxTPphase_gradx,1,*DyTPphase_grady,1);

	delete Dx;
	delete Dy;
	delete DxTPphase_gradx; // If we want to include P.
	//delete DxTphase_gradx;
	delete DyTPphase_grady;
	//delete DyTphase_grady;
	delete phase_gradx_padded;
	delete phase_grady_padded;

	return(EXIT_SUCCESS);
}


