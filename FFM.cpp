#include "MatFileIO.hpp"
#include "FFMOperator.h"
#include "GradReconTools.hpp"
#include "PhaseReconTools.hpp"
#include "Teuchos_XMLParameterListHelpers.hpp"
#include "Tests/Tests.hpp"
#include "Teuchos_Time.hpp"
#include "Amesos.h"
#include "Amesos_BaseSolver.h"
#include "DeconvolutionTools.hpp"
#include "Teuchos_CommandLineProcessor.hpp"
#include "Teuchos_oblackholestream.hpp"


using namespace EpetraExt;

int ReconstructGradient(Teuchos::RCP<Epetra_MultiVector> &X, Teuchos::RCP<Epetra_CrsMatrix> RWpA, const double lambdaScale, const int maxIters, const Epetra_MultiVector *xGradMeas, const Teuchos::RCP<Teuchos::ParameterList> pl, const bool verbose){
	
	/* This fucnction takes in a lot of stuff and then reconstructs the gradient. The gradient reconstruction works
	 * in the same way that the MATLAB version does.
	 */

	int err;
   
	typedef Epetra_MultiVector MV;
	typedef Epetra_Operator OP;
	typedef Belos::MultiVecTraits<double, Epetra_MultiVector> MVT;
	bool ierr;

	if (verbose) {
	  cout << "Gradient Reconstruction " << endl;
	  cout << "Problem info:" << endl;
	  cout << endl;
	}

	int blocksize = 1;
	Teuchos::RCP<Epetra_MultiVector> Bx = Teuchos::rcp( new Epetra_MultiVector(*xGradMeas));
	  
	// Setup the linear problem, with the matrix A and the vectors X and B
	Teuchos::RCP< Belos::LinearProblem<double,MV,OP> > ffmProblem = Teuchos::rcp( new Belos::LinearProblem<double,MV,OP>(RWpA, X, Bx) );
	ffmProblem->setLeftPrec(Teuchos::null);
	
	// Signal that we are done setting up the linear problem
	ierr = ffmProblem->setProblem();

	// Check the return from setProblem(). 
	assert(ierr == true);

		
	// Specify the verbosity level. Options include:
		// Belos::Errors 
		//	 This option is always set
		// Belos::Warnings 
		//	 Warnings (less severe than errors)
		// Belos::IterationDetails 
		//	 Details at each iteration, such as the current eigenvalues
		// Belos::OrthoDetails 
		//	 Details about orthogonality
		// Belos::TimingDetails
		//	 A summary of the timing info for the solve() routine
		// Belos::FinalSummary 
		//	 A final summary 
		// Belos::Debug 
		//	 Debugging information
	int verbosity = Belos::Warnings + Belos::Errors + Belos::FinalSummary + Belos::TimingDetails + Belos::IterationDetails;
	//Belos::IterationDetails;

	// Create the parameter list for the eigensolver
	Teuchos::ParameterList spl = (pl->sublist("LSQR Solver"));
	Teuchos::RCP< Teuchos::ParameterList > lsqrPL = Teuchos::rcp( &spl, false );
	//Teuchos::RCP<Teuchos::ParameterList> myPL = Teuchos::rcp( new Teuchos::ParameterList() );
	double lambda = lsqrPL->get("Lambda", 0.001); //regularization parameter
	lambda = lambda*lambdaScale;
	lsqrPL->set("Lambda", lambda);
	lsqrPL->set( "Verbosity", verbosity);
	lsqrPL->set( "Block Size", blocksize);
	lsqrPL->set("Maximum Iterations", maxIters);
	// myPL->set( "Rel RHS Err", 0.001 ); // stopping criterior parameter, default = 0.
	//lsqrPL->set( "Output Style", 2); // Simple output, the other option is "General"

	// Create the LSQR solver
	// This takes as inputs the linear problem and the solver parameters
	Belos::LSQRSolMgr<double, MV, OP> ffmSolver(ffmProblem, lsqrPL);

	// Solve the linear problem, and save the return code
	Belos::ReturnType solverRet = ffmSolver.solve();

	// Check return code of the solver: Unconverged, Failed, or OK
	switch (solverRet) {
	// UNCONVERGED
		case Belos::Unconverged:
			if (verbose){
				cout << "Belos::BlockLSQRSolMgr::solve() did not converge!" << endl;
			break;
			}

	 // CONVERGED
	 	case Belos::Converged:
			if (verbose)
				cout << "Belos::BlockLSQRSolMgr::solve() converged!" << endl;
			break;
	}

	// Some notes on what it means for LSQR to converge... Taken from BelosLSQRStatusTest.hpp
		/*
		 * Some of the values that it looks at
		 * resid_tol = rel_rhs_err_ + rel_mat_err_ * state.frob_mat_norm * state.sol_norm / state.bnorm;
		 * resid_tol_mach = Teuchos::ScalarTraits< MagnitudeType >::eps() + Teuchos::ScalarTraits< MagnitudeType >::eps() * state.frob_mat_norm * state.sol_norm / state.bnorm;
		 *
		 * stop_crit_1 = state.resid_norm / state.bnorm; 
		 * stop_crit_2 = (state.resid_norm > zero) ? state.mat_resid_norm / (state.frob_mat_norm * state.resid_norm) : zero;
		 * stop_crit_3 = one / state.mat_cond_num; 
		 *
		 *
		 * If any of the following are true, then it has converged.
		 * if (stop_crit_1 <= resid_tol || stop_crit_2 <= rel_mat_err_ || stop_crit_3 <= rcondMin_ || stop_crit_1 <= resid_tol_mach || stop_crit_2 <= Teuchos::ScalarTraits< MagnitudeType >::eps() || stop_crit_3 <= Teuchos::ScalarTraits< MagnitudeType >::eps()) {
		 * termIterFlag = true;
		 */
  
	double iterResNorm = ffmSolver.getResNorm();
	std::vector<double> normB(blocksize);
	MVT::MvNorm( *Bx, normB );

	// Output results to screen
	if(verbose) {
		cout << "******************************************************\n"
			 << "			Results (outside of linear solver)			 \n"
			 << "------------------------------------------------------\n"
			 << "  Linear System\t\tRelative Residual\n"
			 << "------------------------------------------------------\n";
		for( int i=0 ; i<blocksize ; ++i ) {
		  cout << "  " << i+1 << "\t\t\t" << iterResNorm/normB[i] << endl;
		}

		cout << "******************************************************\n" << endl;
	}


	return(EXIT_SUCCESS);

}


int SolveProblem(Epetra_MpiComm &comm, std::ostream &out, int outputVerbosity, int writeVerbosity, std::string params_file){

	int err; // for implementing functions

	// get param list
	Teuchos::RCP< Teuchos::ParameterList > pl = Teuchos::getParametersFromXmlFile(params_file);
	int m = pl->get<int>("m");
	int n = pl->get<int>("n");
	int m_img = pl->get<int>("m_img");
	int n_img = pl->get<int>("n_img");
	int nframes = pl->get<int>("nframes");
	int nlayers = pl->get<int>("nlayers");
	int ssp = pl->get<int>("ssp");
	const char* file_blurred_images = (pl->get<std::string>("file_blurred_images")).c_str();
	const char* file_gradx_meas = (pl->get<std::string>("file_gradx_meas")).c_str();
	const char* file_grady_meas = (pl->get<std::string>("file_grady_meas")).c_str();
	
	const char* file_phaseC_gradx = (pl->get<std::string>("file_phaseC_gradx")).c_str();
	const char* file_phaseC_gradx_var = (pl->get<std::string>("file_phaseC_gradx_var")).c_str();
	const char* file_phaseC_grady = (pl->get<std::string>("file_phaseC_grady")).c_str();
	const char* file_phaseC_grady_var = (pl->get<std::string>("file_phaseC_grady_var")).c_str();
	const char* file_pupil_mask = (pl->get<std::string>("file_pupil_mask")).c_str();
	const char* file_wind_vecs = (pl->get<std::string>("file_wind_vecs")).c_str();
	const char* file_phase_gradx = (pl->get<std::string>("file_phase_gradx")).c_str();
	const char* file_phase_gradx_var = (pl->get<std::string>("file_phase_gradx_var")).c_str();
	const char* file_phase_grady = (pl->get<std::string>("file_phase_grady")).c_str();
	const char* file_phase_grady_var = (pl->get<std::string>("file_phase_grady_var")).c_str();
	const char* file_phase = (pl->get<std::string>("file_phase")).c_str();
	const char* file_phase_var = (pl->get<std::string>("file_phase_var")).c_str();
	const char* file_restored = (pl->get<std::string>("file_restored")).c_str();
	const char* file_restored_var = (pl->get<std::string>("file_restored_var")).c_str();



	double deltax[nlayers];
	double deltay[nlayers];
	double *vals = new double[n*m];
	double wind_vecs[2*nlayers];

	// Read in the wind_vecs. This reads them in as a vector so the first nlayers entries will be the magnitude
	// and the second nlayers entries will be the angles.
	err = ReadMatAsStdVector(file_wind_vecs, wind_vecs, 2*nlayers);
	if (err)
			out << "read in wind_vecs flag is " << err << endl;
	err = ConvertWindVecs(deltax, deltay, wind_vecs, nlayers, ssp); 
	if (err)
			out << "convert to deltax/y flag is " << err << endl;
	err = ReadMatAsStdVector(file_pupil_mask, vals, m*n);
	if (err)
			out << "read in pupil mask flag is " << err << endl;
	
	// Get our processor ID (0 if running serial)
	int MyPID = comm.MyPID();
	// Verbose flag: only processor 0 should print
	bool verbose = (MyPID==0);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GRADIENT RECONSTRUCTION
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   
	// generate Epetr_CrsMatrix pointer
	Epetra_CrsMatrix *A = NULL;
	err = ConstructMotionMatrix(A,m,n,nframes,nlayers,deltax,deltay,comm);
	if((outputVerbosity>0) || err)
		out << "Flag of constructing motion matrix is " << err << endl;

	// generate the pupil mask matrix (small ones for each frames)
	Epetra_CrsMatrix *Wp = NULL;
	err = ConstructDiagonalPupilMatrix(Wp,m,n,nframes,nlayers,deltax,deltay,vals,comm);
	if((outputVerbosity>0) || err)
		out << "Flag of constructing the window matrix is " << err << endl;

	// generate the downsampling matrix  (small ones for each frames)
	Epetra_CrsMatrix *R = NULL;
	err = ConstructDiagonalDownSampleMatrix(R,m,n,nframes,nlayers,deltax,deltay,ssp,comm);
	if((outputVerbosity>0) || err)
		out << "Flag of constructing the downsampling matrix is " << err << endl;

//	Epetra_CrsMatrix *WpA = NULL;
//	err = ConstructPupilMotionMatrix(WpA,m,n,nframes,nlayers,deltax,deltay,vals,comm);
//	if((outputVerbosity>0) || err)
//		out << "Flag of constructing window times motion matrix is " << err << endl;
	
	// Perform the multiplication to creat the whole operator
	Teuchos::Time matMultTimer = Teuchos::Time("Mat Mult Time",true);	
	Epetra_CrsMatrix *WpA = new Epetra_CrsMatrix(Copy,Wp->RowMap(),4*nlayers);
	Teuchos::RCP<Epetra_CrsMatrix> RWpA = Teuchos::rcp(new Epetra_CrsMatrix(Copy,R->RowMap(),0));
	EpetraExt::MatrixMatrix::Multiply(*Wp,false,*A,false,*WpA,true);
	EpetraExt::MatrixMatrix::Multiply(*R,false,*WpA,false,*RWpA,true);
	matMultTimer.stop();
	if(outputVerbosity>0){
		out << matMultTimer.name() << ": " << matMultTimer.totalElapsedTime() << endl;
	}
	// testing

	// read in the measured data
	// Declaration of vectors.
	Epetra_MultiVector *xGradMeas = NULL;
	Epetra_MultiVector *yGradMeas = NULL;

	// define the map for the reading in data and load the data
	Epetra_Map range(R->RangeMap()); 

	err = ReadMatAsMultiVector(file_gradx_meas, comm, xGradMeas, range);
	if ((outputVerbosity>0) || err)
		out << "read in file_gradx flag is " << err << endl;

	err = ReadMatAsMultiVector(file_grady_meas, comm, yGradMeas, range);  
	if ((outputVerbosity>0) || err)
		out << "read in file_grady flag is " << err << endl;

	double lambdaScale;
	err = CalculateReguParamScale(lambdaScale, A);
	if(err) return err;

	int mLarge, nLarge;
	err = GetCompositeImageSize(&mLarge, &nLarge, m, n, deltax, deltay, nframes, nlayers);
	int maxIters = 10*mLarge;
	
	// Setup the vectors to be returned here
	int blocksize = 1;
	const Epetra_Map * XMap = &(WpA->DomainMap());
	Teuchos::RCP<Epetra_MultiVector> phaseC_gradx = Teuchos::rcp(new Epetra_MultiVector(*XMap,blocksize));
	Teuchos::RCP<Epetra_MultiVector> phaseC_grady = Teuchos::rcp(new Epetra_MultiVector(*XMap,blocksize));
	// Initialize the solution with zero and right-hand side with random entries
	phaseC_gradx->PutScalar( 0.0 );
	phaseC_grady->PutScalar( 0.0 );


	// Reconstruct it!!!
	//////////////////
	out << "Reconstructing Gradient..." << endl;


	Teuchos::Time gradReconTimer = Teuchos::Time("Gradient Recon Time",true);
	// This is a bit of a hack to capture the output to a string so that we don't need the
	// very verbose output from LSQR
	std::streambuf* oldCoutStreamBuf = cout.rdbuf();
	std::ostringstream strCout;
	if(outputVerbosity < 2){
		cout.rdbuf( strCout.rdbuf() );
	}

	err = ReconstructGradient(phaseC_gradx, RWpA, lambdaScale, maxIters, xGradMeas, pl, verbose);
	if(err) return err;
	
	err = ReconstructGradient(phaseC_grady, RWpA, lambdaScale, maxIters, yGradMeas, pl, verbose);
	if(err) return err;

	if(outputVerbosity<2){
		std::cout.rdbuf( oldCoutStreamBuf ); // end hackery
	}


	out << "Gradient Reconstruction Done!" << endl;

	gradReconTimer.stop();
	if(outputVerbosity>0){
		out << gradReconTimer.name() << ": " << gradReconTimer.totalElapsedTime() << endl;
	}


	// Write the solution to disk.
	if(writeVerbosity>0){
		err = SaveMultiVector(file_phaseC_gradx, file_phaseC_gradx_var, *phaseC_gradx);
		if(err) return err;

		err = SaveMultiVector(file_phaseC_grady, file_phaseC_grady_var, *phaseC_grady);
		if(err) return err;
	}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PHASE RECONSTRUCTION
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	// Set up multivectors for punching out frames
	//const Epetra_Map *phaseGradMap = &(WpA->RangeMap());
	const Epetra_Map *phaseGradMap = &(WpA->RangeMap());
	Teuchos::RCP<Epetra_MultiVector> phase_gradx = Teuchos::rcp(new Epetra_MultiVector(*phaseGradMap,blocksize));
	Teuchos::RCP<Epetra_MultiVector> phase_grady = Teuchos::rcp(new Epetra_MultiVector(*phaseGradMap,blocksize));

	err = PunchOutFrames(phase_gradx, WpA, phaseC_gradx);
	if(err) return err;

	err = PunchOutFrames(phase_grady, WpA, phaseC_grady);
	if(err) return err;

	// Write punched out phases
	if(writeVerbosity>0){
		err = SaveMultiVector(file_phase_gradx, file_phase_gradx_var, *phase_gradx);
		if(err) return err;

		err = SaveMultiVector(file_phase_grady, file_phase_grady_var, *phase_grady);
		if(err) return err;
	}


	// Create pad matrix, Phase Reconstruction matrix (normal eqns) and RHS
	double reguParam = 1e-6; //hardcoded for now from one of the tests...	
	int const padSize = 1;

	// Construct necessary objects
	Epetra_CrsMatrix *PadMat = NULL;
	err = ConstructPadMatrix(PadMat, m, n, padSize, comm);
	if(err) return err;

	Epetra_CrsMatrix *P = NULL;
	err = ConstructPhasePupilMatrix(P,m,n,vals,padSize,PadMat, comm);
	if(err) return err;

	Epetra_CrsMatrix *PhaseReconMat = NULL;
	err = ConstructPhaseReconMatrix(PhaseReconMat, m, n, padSize, P, reguParam, comm);

	Epetra_MultiVector *PhaseRHS = NULL;
	err = ConstructPhaseRHS(PhaseRHS, m, n, padSize, nframes, phase_gradx, phase_grady, PadMat, P, comm);

	// Solution multivec
	Epetra_MultiVector *phase_padded = new Epetra_MultiVector(PhaseReconMat->DomainMap(),nframes); 
	phase_padded->PutScalar( 0.0 );

	// Set up the linear problem and then solve it.
	out << "Reconstructing phase..." << endl;

	Teuchos::RCP<Epetra_LinearProblem> phaseReconProblem = Teuchos::rcp( new Epetra_LinearProblem(PhaseReconMat,phase_padded,PhaseRHS));
	Amesos_BaseSolver* Solver;
	Amesos Factory;
	char* SolverType = "Amesos_Superludist";
	Solver = Factory.Create(SolverType, *phaseReconProblem);


	Teuchos::Time phaseReconTimer = Teuchos::Time("Phase Recon Time",true);

	if(outputVerbosity < 2){ //hack to redirect output
		cout.rdbuf( strCout.rdbuf() );
	}
	

	Teuchos::Time symbFactTimer = Teuchos::Time("symbFact Time",true);
	err = Solver->SymbolicFactorization();
	if(err) cout << "err sym" << endl;
	symbFactTimer.stop();

	Teuchos::Time numFactTimer = Teuchos::Time("numFact Time",true);
	err = Solver->NumericFactorization();
	if(err) cout << "err numer" << endl;
	numFactTimer.stop();

	Teuchos::Time phaseSolveTimer = Teuchos::Time("phaseSolve Time",true);
	err = Solver->Solve();
	if(err) cout << "err solve" << endl;
	phaseSolveTimer.stop();

	if(outputVerbosity<2){
		std::cout.rdbuf( oldCoutStreamBuf ); // end hackery
	}


	// Apply pupil mask
	Epetra_MultiVector *phase_punched = new Epetra_MultiVector(P->RangeMap(),nframes);
	err = P->Multiply(false,*phase_padded,*phase_punched);
	if(err) return err;

	// Unpad the solution.
	Epetra_MultiVector *phase = new Epetra_MultiVector(PadMat->DomainMap(),nframes);
	err = PadMat->Multiply(true,*phase_punched,*phase);
	if(err) return err;


	out << "Phase Reconstruction complete!" << endl;

	phaseReconTimer.stop();
	if(outputVerbosity>0){
		out << symbFactTimer.name() << ": " << symbFactTimer.totalElapsedTime() << endl;
		out << numFactTimer.name() << ": " << numFactTimer.totalElapsedTime() << endl;
		out << phaseSolveTimer.name() << ": " << phaseSolveTimer.totalElapsedTime() << endl;
		out << phaseReconTimer.name() << ": " << phaseReconTimer.totalElapsedTime() << endl;
	}

	comm.Barrier();

	// save solution to disk
	if(writeVerbosity>0){
		err = SaveMultiVector(file_phase, file_phase_var, *phase);
		if(err) return err;
	}

	comm.Barrier();

	delete PhaseRHS;
	delete PhaseReconMat;
	delete PadMat;
	delete A;
	delete Wp;
	delete R;
	delete xGradMeas;
	delete yGradMeas;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// IMAGE DECONVOLUTION
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	out << "Starting deconvolution..." << endl;
	// Determine the number of frames that need to be stored on each processor. All processes will have this info.
	int framesPerProc[comm.NumProc()];
	for(int i=0;i<comm.NumProc();++i){
		int extra = ((i >= nframes%(comm.NumProc())) ? 0 : 1);
		framesPerProc[i] = nframes/(comm.NumProc()) + extra;
	}
	
	////////////////////////////////////////////////////////////////
	// read in the image as a std C++ array and distribute the data
	////////////////////////////////////////////////////////////////
	
	int size = m_img*n_img; // Size of the image, i.e. number of rows times number of columns times number of frames

	double** g_arr_multidim = new double*[nframes];

	if(comm.MyPID() == 0){
		double *g_arr = new double[size*nframes];
		err = ReadMatAsStdVector(file_blurred_images, g_arr, size*nframes);
		if(err) cout << "read err" << endl;

		for(int i=0;i<nframes;++i){
			g_arr_multidim[i] = new double[size];
		}
		
		err = Reshape2DArray(g_arr, size*nframes, 1, g_arr_multidim, size, nframes);
		if(err) return err;

	}



	// Distribute the image data for all of the frames. First initaialize the proper number of arrays for each processor.
	double** g_arr_local = new double*[framesPerProc[comm.MyPID()]];
	for(int j=0;j<framesPerProc[comm.MyPID()];++j){
		g_arr_local[j] = new double[size];
	}

	// Copy data from process 0 to process 0 manually. Really just copy the pointer to the beginning
	// of the data for each frame.
	if(comm.MyPID() == 0){
		for(int i=0;i<framesPerProc[0];++i){
			for(int j=0;j<size;++j){
				g_arr_local[i][j] = g_arr_multidim[i][j];
			}
		}
	}


	// now that the recv buffers have been allocated, we can send the data to the recv buffers.
	// Only send data to processes 1-NumProcs. Sending 0 to 0 does not work. We only have to do 
	// this step if we have more than one process
	MPI_Status status;
	if(comm.NumProc() > 1){
		int frameNumber = framesPerProc[0]; // remember we already did all of the frames for process 0.
		for(int i=1;i<comm.NumProc();++i){ // can't do 0...
			for(int j=0;j<framesPerProc[i];++j){
			//	cout << frameNumber << endl;
			//	cout << comm.MyPID() << endl;
				if(comm.MyPID() == 0){
					// Send only from proc 0, send to proc i, tag it with the overall frameNumber
					//cout << "send from " << comm.MyPID() << endl;
					MPI_Send(g_arr_multidim[frameNumber],size,MPI_DOUBLE,i,frameNumber,comm.Comm());
				}
				if(comm.MyPID() == i){
					//cout << "recv" << endl;	
					MPI_Recv(g_arr_local[j],size,MPI_DOUBLE,0,frameNumber,comm.Comm(),&status);
					//cout << status.MPI_ERROR << endl;

				}
				frameNumber += 1;
			}
		}
	}


	//////////////////////////////////////////////////////////////////
	// Convert phase to a standard C++ array and distribute
	//////////////////////////////////////////////////////////////////
	double** phase_std = new double*[phase->NumVectors()]; // NumVectors should be nframes here
	if(comm.MyPID() == 0){
		for(int i=0;i<phase->NumVectors();++i){
			phase_std[i] = new double[phase->GlobalLength()];
		}
	}
	err = MultiVectorToArray(phase_std, *phase, false);
	if(err) return err;


	// Now all of the data needs to be distributed. We must first initialize the proper number
	// of arrays for each process to be the recv buffer
	double** phase_local = new double*[framesPerProc[comm.MyPID()]];
	for(int j=0;j<framesPerProc[comm.MyPID()];++j){
		phase_local[j] = new double[phase->GlobalLength()];
	}
	comm.Barrier();

	// Copy data from process 0 to process 0 manually. Really just copy the pointer to the beginning
	// of the data for each frame.
	if(comm.MyPID() == 0){
		for(int i=0;i<framesPerProc[0];++i){
			for(int j=0;j<phase->GlobalLength();++j){
				phase_local[i][j] = phase_std[i][j];
			}
		}
	}
	
	// now that the recv buffers have been allocated, we can send the data to the recv buffers.
	// Only send data to processes 1-NumProcs. Sending 0 to 0 does not work. We only have to do 
	// this step if we have more than one process
	if(comm.NumProc() > 1){
		int frameNumber = framesPerProc[0]; // remember we already did all of the frames for process 0.
		for(int i=1;i<comm.NumProc();++i){ // can't do 0...
			for(int j=0;j<framesPerProc[i];++j){
			//	cout << frameNumber << endl;
			//	cout << comm.MyPID() << endl;
				if(comm.MyPID() == 0){
					// Send only from proc 0, send to proc i, tag it with the overall frameNumber
					//cout << "send from " << comm.MyPID() << endl;
					MPI_Send(phase_std[frameNumber],phase->GlobalLength(),MPI_DOUBLE,i,frameNumber,comm.Comm());
				}
				if(comm.MyPID() == i){
					//cout << "recv" << endl;	
					MPI_Recv(phase_local[j],phase->GlobalLength(),MPI_DOUBLE,0,frameNumber,comm.Comm(),&status);
					//cout << status.MPI_ERROR << endl;

				}
				frameNumber += 1;
			}
		}
	}


	////////////////////////////////////////////////////////////////////
	// Pad phase and pupil mask to have same size as blurred image
	////////////////////////////////////////////////////////////////////

	int nrow_g, ncol_g, nrow_phi, ncol_phi, nrow_pm, ncol_pm, phi_padsize, pm_padsize;
	nrow_g = ncol_g = sqrt(size);
	nrow_phi = ncol_phi = sqrt(phase->GlobalLength());
	nrow_pm = ncol_pm = sqrt(m*n);
	phi_padsize = (nrow_g - nrow_phi)/2, pm_padsize = (nrow_g - ncol_pm)/2;


	//phi = padarray(phi, [phi_padsize, phi_padsize], 'both');
	int xSize[] = {nrow_phi, ncol_phi};
	int padSizeD[] = {phi_padsize, phi_padsize}; //renamed because of conflict
	double **padPhi = new double*[framesPerProc[comm.MyPID()]];
	for(int i=0;i<framesPerProc[comm.MyPID()];++i){
		padPhi[i] = new double[nrow_g*ncol_g](); // empty parenthesis acts as default initializer. Basically magic. See
				// http://stackoverflow.com/questions/2204176/how-to-initialise-memory-with-new-operator-in-c
		padarray(phase_local[i], xSize, padPhi[i], padSizeD); 
	}
	

	//pupil_mask = padarray(pupil_mask, [pm_padsize, pm_padsize], 'both');
	padSizeD[0] = pm_padsize, padSizeD[1] = pm_padsize;
	xSize[0] = nrow_pm, xSize[1] = ncol_pm;
	double *padPm =  new double[nrow_g*ncol_g]();

	padarray(vals, xSize, padPm, padSizeD);

	//pupil_mask = nrow_g*pupil_mask/sqrt(sum(pupil_mask(:)));
	double pupil_mask_sum;
	for(int i=0;i<m*n;++i){
		pupil_mask_sum += vals[i];
	}
	double norm = nrow_g/sqrt(pupil_mask_sum);
	for(int i=0;i<nrow_g*ncol_g;++i){
		padPm[i] *= nrow_g;
		padPm[i] /= sqrt(pupil_mask_sum);
	}


	//////////////////////////////////////////////////////////////////////////////////////////////
	// Start the deconvolution process. First we need to calculate the PSF (one for each frame)
	// from the pupil_mask and the phase (padded). Then using the PSF we can calculate S and g_hat
	// where S is the matrix such that PSF = F*xSxF where F and F* are fourier transform matrices. 
	// g_hat is the fourier transform applied to each frame of the input image data. After that 
	// we calculate sum(conj(S).*g_hat) and sum(abs(S)^2)+alpha^2 where alpha is a regularization
	// parameter (for now hardcoded to be 1e-4) and the sums range over all the frames. From there
	// we perform an element wise division [sum(conj(S).*g_hat)]./[sum(abs(S)^2)+alpha^2] and finally
	// perform an inverse fourier transform on that to complete the deconvolution
	///////////////////////////////////////////////////////////////////////////////////////////////
	

	Teuchos::Time deconvTimer = Teuchos::Time("Deconvolution Time",true);

	// Construct the PSF, one for each frame
	double **PSF = new double*[framesPerProc[comm.MyPID()]];
	for(int i=0;i<framesPerProc[comm.MyPID()];++i){
		PSF[i] = new double[size];
		err = ConstructPSF(PSF[i], ncol_g, nrow_g, size, padPm, padPhi[i]);
	}


	// Construct S and g_hat. We need one for each frame that is on this processor
	int csize = nrow_g* (ncol_g/2 + 1);
	fftw_complex **S = new fftw_complex*[framesPerProc[comm.MyPID()]];
	fftw_complex **g_hat = new fftw_complex*[framesPerProc[comm.MyPID()]];
	for(int i=0;i<framesPerProc[comm.MyPID()]; ++i){
		S[i] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*csize);
		g_hat[i] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*csize);

		fft2(PSF[i], S[i], nrow_g, ncol_g);
		fft2(g_arr_local[i], g_hat[i], nrow_g, ncol_g);	
	}

	// For each process we need to calculate the sum(conj(S).*g_hat) for all of the frames taht are on that process.

	fftw_complex* sumConjS_gHat = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*csize);
	memset(sumConjS_gHat,0,sizeof(fftw_complex)*csize);

	double* sumAbsSsqr = new double[csize];
	memset(sumAbsSsqr,0,sizeof(double)*csize);

	//cout<< "frames on " << comm.MyPID() << " = " << framesPerProc[comm.MyPID()] << endl;

	for(int k=0;k<framesPerProc[comm.MyPID()];++k){

		fftw_complex *Sk = S[k];
		fftw_complex *g_hatk = g_hat[k];
		int ij = 0;

		for (int i = 0; i < nrow_g; ++i){
			for (int j = 0; j < (ncol_g/2 + 1); ++j) {
				ij = i*(ncol_g/2.0 +1) + j;
				sumAbsSsqr[ij] += ((Sk[ij])[0]*(Sk[ij])[0] + (Sk[ij])[1]*(Sk[ij])[1]);

				(sumConjS_gHat[ij])[0] += (((Sk[ij])[0] * (g_hatk[ij])[0] + (Sk[ij])[1] * (g_hatk[ij])[1]));

				(sumConjS_gHat[ij])[1] += (((Sk[ij])[0] * (g_hatk[ij])[1] - (Sk[ij])[1] * (g_hatk[ij])[0]));

			}
		}
	}


	// reduce the sum of abs(S)^2 and S.*g_hat
	double *outAbs = new double[csize];
	fftw_complex* outConj = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*csize);

	MPI_Reduce(sumAbsSsqr,outAbs,csize,MPI_DOUBLE,MPI_SUM,0,comm.Comm());

	MPI_Reduce(sumConjS_gHat,outConj,csize,MPI_DOUBLE_COMPLEX,MPI_SUM,0,comm.Comm());


	//alpha = eps;
	double alpha = 1e-4; // HARDCODED regu parameter. This needs to be fixed.
	double alphaSqr = alpha*alpha;

	// add alpha squared and then do element was division. Need to just do this once for the sum
	// of everything, so we do it only on process 0.
	if(comm.MyPID() == 0){
		double denom = 0;
		int ij = 0;
		for (int i = 0; i < nrow_g; ++i){
			for (int j = 0; j < (ncol_g/2 + 1); ++j) {
				ij = i*(ncol_g/2.0 +1) + j;
				denom = outAbs[ij] + alphaSqr;
				//cout << (outConj[ij][0]) << endl;
				(outConj[ij])[0] /= denom;
				(outConj[ij])[1] /= denom;

			}
		}	
	

		double scale = 1.0/(size);
		//cout << scale << endl;
		double *f_tikV = (double*) malloc(sizeof(double)*size);

		ifft2(outConj, f_tikV, nrow_g, ncol_g);
		
		// Perform the final scaling
		for(int i =0;i<size;i++){
			f_tikV[i] *= scale;
		}
		

		// Save the restored image to the disk
		double** f_tikV_reshape = new double*[ncol_g];
		for(int i=0;i<ncol_g;++i){
			f_tikV_reshape[i] = new double[nrow_g];
		}

		err = Reshape2DArray(f_tikV, size, 1, f_tikV_reshape, ncol_g, nrow_g);
		if(err) return err;
		
		int f_tikV_shape[] = {ncol_g, nrow_g};
		WriteMatArray(file_restored, file_restored_var, f_tikV_reshape, f_tikV_shape);
	}


	out << "Deconvolution done!" << endl;

	deconvTimer.stop();
	if(outputVerbosity>0){
		out << deconvTimer.name() << ": " << deconvTimer.totalElapsedTime() << endl;
	}
	

	fftw_free(outConj);
	delete[] outAbs;
//	delete[] outConj;
	delete phase_padded; // Want to free it early?


	delete phase;	
	delete Solver;
	delete[] vals; // may want to consider freeing this early?

	return(EXIT_SUCCESS);
}

int main(int argc, char **argv)
{	
	
	#ifdef EPETRA_MPI
		MPI_Init(&argc,&argv);
		Epetra_MpiComm comm(MPI_COMM_WORLD);
	#else
		Epetra_SerialComm comm;
	#endif

//	MultiplyTest(comm, argv[1]);
/*
	if (argc>=2) {
		SolveProblem(comm, argv[1]);
	}
	else {
		cout << "Need Data" << endl;
	}

*/
	//set up output streams
	Teuchos::oblackholestream blackhole;
	std::ostream &out = ( comm.MyPID() == 0 ? std::cout : blackhole );
	
	//parse inputs!!
	Teuchos::CommandLineProcessor clp;
	clp.recogniseAllOptions(true);
	clp.throwExceptions(false);

   	clp.setDocString(
    	"FFM program. More to come here."
 	);

	int outputVerbosity = 0;
	clp.setOption("output-verbosity", &outputVerbosity, "How verbose the output should be. 0 is the least, 1 is slightly, 2 is very.");

	int writeVerbosity = 0;
	clp.setOption("write-verbosity", &writeVerbosity, "How much to write to disk. 0 for nothing but the final solution, 1 for everything");
	
	std::string problem = "";
	clp.setOption("problem", &problem, "Relative path to the XML file defining the problem information");

	bool isTest = false;
	clp.setOption("test","no-test", &isTest, "Indicate whether or not this is a test problem. If it as, writeVerbosity is set to 1.");


	Teuchos::CommandLineProcessor::EParseCommandLineReturn parseReturn= clp.parse( argc, argv );
	if( parseReturn == Teuchos::CommandLineProcessor::PARSE_HELP_PRINTED ) {
		return 0;
	}
	if( parseReturn != Teuchos::CommandLineProcessor::PARSE_SUCCESSFUL ) {
		return 1; // Error!
	}

	if(isTest){
		writeVerbosity=1;
	}


	// Solve the problem
	SolveProblem(comm, out, outputVerbosity, writeVerbosity, problem);
	if(isTest){
		FFMTest(comm, out, outputVerbosity, problem);
	}


	#ifdef HAVE_MPI
		MPI_Finalize();
	#endif

	return(EXIT_SUCCESS);
}

