#include <valarray>
#include <complex>
#include <fftw3.h>
#include <limits>
#include <cmath>


template<class Scalar>
void fft2(Scalar* &x, fftw_complex * &result, int M, int N)
{	
	/* template<class Scalar>
	 *void fft2(Scalar* &x, fftw_complex * &result, int M, int N)
	 *
	 * This function performs a 2D real to complex DFT on input x and stores the result in result.
	 *
	 * INPUTS
	 *      -x - reference to an array of Scalar types
	 *      -result - reference to an array of complex. The array type is fftw_complex because this
	 *      	  is what FFTW wants. (sorry no std::complex). This is the output
	 *     	-M,N - integers, these give the dimensions of the array that we are performing the 2D dft on.
	 *     	       Note that although we have 2 inputs representing the 2D array size, the inputs and 
	 *     	       outputs are all 1D arrays of scalars or complex
	 */

	int size = M*N;

	/*
	 * planner routine for 2D DFT of real data
	 * the complex output array is cut roughly in half
	 * if the input (a in this example) is of size m x n then the output after the r2c transform will be an m x (n/2 + 1) array of fftw_complex
	 * values - A in our example
	 * the transforms are not normalized => a r2c followed by a c2r will result in the original data scaled by the number of real data elements (i.e. mxn)
	 */
	fftw_plan p = fftw_plan_dft_r2c_2d( M, N, x,result,FFTW_ESTIMATE);

	/*
	 * execute the plan
	 * compute the corresponding transform on the array for which it was planned above
	 * the plan -p- is not modified and fftw_execute can be called as many times as desired
	 */
	fftw_execute(p);

	fftw_destroy_plan(p);
}

template<class Scalar>
void ifft2(fftw_complex *&X, Scalar *result, int M, int N)
{

	/* template<class Scalar>
	 *void ifft2(fftw_complex* &X, Scalar *result, int M, int N)
	 *
	 * This function performs a 2D real to complex inverse DFT on input X and stores the result in result.
	 *
	 * INPUTS
	 *      -X - reference to an array of fftw_complex (typically produced via output from a forward 2D DFT)
	 *      *      -result - scalar array (I'm not passing as a reference, but arrays are passed by reference 
	 *      		 in C++ by default unless const, I think. Hopefully shouldn't be problematic)
	 *     	-M,N - integers, these give the dimensions of the array that we are performing the 2D dft on.
	 *     	       Note that although we have 2 inputs representing the 2D array size, the inputs and 
	 *     	       outputs are all 1D arrays of scalars or complex
	 */

	int size = M*N;


	fftw_plan pinv = fftw_plan_dft_c2r_2d(M, N,X ,result,FFTW_ESTIMATE);

	fftw_execute(pinv);
	fftw_destroy_plan(pinv);

}


template<class Scalar>
void padarray(Scalar *x, int *xSize, Scalar *&padX, int *padSize)

	/*template<class Scalar>
	 * void padarray(Scalar *x, int *xSize, Scalar *&padX, int *padSize)
	 *
	 * This function pads a standard 1D C++ array of type scalar.
	 *
	 * INPUTS
	 * 	- x - array of type scalar, the input that needs to be padded
	 * 	- xSize - (I didn't write this, but I think it gives the dimensions of x
	 * 		   as a 2D array)
	 * 	- padX - reference to an array of type scalar. This is the padded version of x (output)
	 * 	- padSize - (again, didn't write this, but I think that this tells how much padding we need
	 * 			along each dimension)
	 */

{
	int k = 0;
	int idx = (xSize[0] + 2*padSize[0])*padSize[0] + padSize[1];
	for(int i = 0; i < xSize[0]; i++){
		for(int j = 0; j < xSize[1]; j++){
			padX[idx] = x[k];
			k++;
			idx++;
		}
		idx += padSize[1]*2;
	}

}


template<class Scalar>
int ConstructPSF(Scalar *&newPSF, const int ncol_g, const int nrow_g, const int size, const Scalar* padPm, const Scalar* padPhi){

	/* template<class Scalar>
	 * int ConstructPSF(Scalar *&newPSF, const int ncol_g, const int nrow_g, const int size, Scalar* padPm, const Scalar* padPhi)
	 * 
	 * This function creates the PSF from the padded phase data and the padded pupil mask.
	 *
	 * INPUTS
	 * 	- newPSF - reference to an array of type Scalar, this will be the output PSF
	 * 	- ncol_g, nrow_g - an integer, this is the number of columns/rows of the input image
	 * 	- size - int, I guess redundant because this should just be nrow_g*ncol_g
	 * 	 padPm - array of type Scalar, the padded pupil mask
	 * 	 padPhi - array of type scalar, padded phase
	 */

	//
	/*
	 * (c) Construct PSF from phase and pupil mask
	 * NOTES: (i)  To make sure the PSF is normalized so that its entries
	 * 				sum to one, we scale the pupil mask.
	 * 		  (ii) The way the PSF is constructed, it is already in
	 * 		  		circshifted form.
	 */



	//PSF = abs(ifft2(pupil_mask.*exp(sqrt(-1)*phi)) ).^2;
	std::complex<double> *pm_exp = new std::complex<double>[nrow_g*ncol_g];
	for(int i = 0; i < size; ++i){
	//	cout << "idx is: " << i << endl;
		pm_exp[i] = std::complex<double>(0, padPhi[i]);
		pm_exp[i] = exp(pm_exp[i]);
	}


	//pm_exp = exp(pm_exp);

	for(int i=0;i<size;i++)
		pm_exp[i] = std::complex<double>(pm_exp[i].real()*padPm[i], pm_exp[i].imag()*padPm[i]);

	fftw_complex *psf = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*size);

	fftw_complex *fftPSF = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*size);
	double scale = 1.0/size;

	for(int i = 0; i < size; i++){
		fftPSF[i][0] = pm_exp[i].real();
		fftPSF[i][1] = pm_exp[i].imag();
	}

	fftw_plan plan_backward = fftw_plan_dft_2d(nrow_g, ncol_g, fftPSF, psf, FFTW_BACKWARD, FFTW_ESTIMATE);

	fftw_execute(plan_backward);

	for(int i = 0; i < size; i++){
		psf[i][0] *= scale;
		psf[i][1] *= scale;
	}

	fftw_destroy_plan(plan_backward);

	std::complex<double> vPSF[size];
	for(int i = 0; i < size; i++)
		vPSF[i] = std::complex<double>(psf[i][0], psf[i][1]);

//	std::valarray<double> newPSF(size);
	for(int i = 0; i < size; i++)
		newPSF[i] = pow(sqrt(vPSF[i].real()*vPSF[i].real() + vPSF[i].imag()*vPSF[i].imag()),2.0);

//	newPSF = pow(newPSF, 2.0);
	delete[] pm_exp;
	fftw_free(psf);
	fftw_free(fftPSF);

	return(EXIT_SUCCESS);

}


