Usage Instructions

Once everything has been compiled, you can run the software as follows (for example):

	mpirun -np 1 FFM --problem=Tests/XML/Test-10F1L01N.xml --test --output-verbosity=1 --write-verbosity=1

Use
	mpirun -np 1 FFM --help
for more information on the options.

This will run a small test problem. Examining the XML file in the above example
will give information about where the files being read in and those being saved are
read from or written too. Note also that since these are part of tests, running an
actual problem will require a little less information, but at present the software
is written to always run a test problem (so if no truth info is present to compare 
against, you'll get an error).

There are many other tests you included in the Tests folder. They are organized as
	#F - Number of frames
	#L - Number of layers
	#N - Amount of noise
So Test-25F3L01N.xml is for a test problem with 25 frames, 3 layers and 1% noise.
There are some multiply tests in there as well, but you'll have to edit the source
to run those (and some are out of date).

For the time being I am capturing the output from LSQR because I don't want to see it.
I think it should be an option in the future.

At present the phase reconstruction is not working correctly. There are some checkerboarding
artifacts on the entire reconstruction.
