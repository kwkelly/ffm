#ifndef _AL_SPMV_H
#define _AL_SPMV_H

#include <cstring>
#include <cstdio>
#include <iostream>
#include <fstream>

#include <Epetra_ConfigDefs.h>

#ifdef EPETRA_MPI
#include <mpi.h>
#include <Epetra_MpiComm.h>
#else
#include <Epetra_SerialComm.h>
#endif

#include <Epetra_Import.h>
#include <Epetra_Map.h>
#include <Epetra_LocalMap.h>
#include <Epetra_CrsGraph.h>
#include <Epetra_CrsMatrix.h>
#include <Epetra_MultiVector.h>
#include "Epetra_Operator.h"

#include <EpetraExt_MatrixMatrix.h>
#include <EpetraExt_BlockMapIn.h>
#include <EpetraExt_CrsMatrixIn.h>
#include <EpetraExt_RowMatrixOut.h>
#include <EpetraExt_MultiVectorIn.h>
#include <EpetraExt_MultiVectorOut.h>
#include <EpetraExt_VectorIn.h>

#include "Epetra_Util.h"
#include "Epetra_BlockMap.h"
#include "Epetra_IntVector.h"
#include "Epetra_IntSerialDenseVector.h"
#include "Epetra_Import.h"
#include "Epetra_Export.h"
#include "EpetraExt_mmio.h"

#include "BelosConfigDefs.hpp"
#include "BelosMultiVec.hpp"
#include "BelosOperator.hpp"
#include "BelosTypes.hpp"
#include "BelosLinearProblem.hpp"
#include "BelosEpetraAdapter.hpp"
#include "BelosLSQRSolMgr.hpp"

class FFM: public virtual Epetra_Operator {
private:
  typedef Epetra_MultiVector				MV;
  typedef Epetra_CrsMatrix				  MTRX;
  typedef Epetra_Map					   MAP;

	MTRX WpA;
	//MTRX Wp;
	MTRX R;
	MAP  *mapWpA;
//	MAP  *mapWp;
	MAP  *mapR;
	MV	 *WpAx;
//	MV	 *WAx;
	MV	 *RTx;
//	MV	 *WTRTx;
 
  bool UseTranspose_;

public:

  FFM( const MTRX &WpA_, const MTRX &R_):
	WpA(WpA_), R(R_){
		mapWpA = new MAP(WpA.DomainMap());
		//mapWp = new MAP(Wp.RangeMap());
		mapR = new MAP(R.RangeMap());
		WpAx = new MV(WpA.RangeMap(), 1); // since we only have 1 vector
		//WAx = new MV(Wp.RangeMap(), 1);
		RTx = new MV(R.DomainMap(), 1);
		//WTRTx = new MV(Wp.DomainMap(), 1);
	}

  ~FFM(){
	  delete mapWpA;
	  delete mapR;
	  delete WpAx;
	  delete RTx;
	  };

  int SetUseTranspose(bool UseTranspose_in) {UseTranspose_ = UseTranspose_in; return(0);}

  int Apply(const Epetra_MultiVector &, Epetra_MultiVector &) const;

  int ApplyInverse( const Epetra_MultiVector &, Epetra_MultiVector &) const{
	return (-1); // Not implemented.
  }

  double NormInf() const {
	return(-1);
  }

  const char* Label() const {
	return("Q verstion of matrix-vector multiplication");
  }

  bool UseTranspose() const {return(UseTranspose_);}

  bool HasNormInf () const {
	return(false);
  }

  const Epetra_Comm & Comm() const {
	return(WpA.Comm());
  }

  const Epetra_Map & OperatorDomainMap() const {
	 // This the sentence which bother me most when print out the output info.
	//cout << "Domain map" << endl << *mapA << endl;
	return(*mapWpA);
  }

  const Epetra_Map & OperatorRangeMap() const {
	 // This the sentence which bother me most when print out the output info.
	//cout << "Range map" << endl << *mapA << endl;
	return(*mapR);
  }

  // Other function.
  int Multiply( bool , const Epetra_MultiVector &, Epetra_MultiVector &) const;

};
#endif
