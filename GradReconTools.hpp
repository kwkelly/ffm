#define NOCOL 256 
#include <iostream>
#include "Epetra_ConfigDefs.h"
#ifdef HAVE_MPI
#include "mpi.h"
#include "Epetra_MpiComm.h"
#else
#include "Epetra_SerialComm.h"
#endif
#include "Epetra_Map.h"
#include "Epetra_Vector.h"
#include "Epetra_CrsMatrix.h"
#include "EpetraExt_CrsMatrixIn.h"
#include "AztecOO.h"
#include <EpetraExt_BlockMapIn.h>
#include <EpetraExt_CrsMatrixIn.h>
#include "Epetra_MultiVector.h"
#include <EpetraExt_MultiVectorIn.h>
#include <EpetraExt_MultiVectorOut.h>
#include <algorithm>

using namespace EpetraExt;

int Vectorize(const double image[][NOCOL], double *vec, const int m, const int n)
{
	for(int i=0; i<m; ++i)
		for(int j=0; j<n; ++j)
			vec[i+j*n] = image[i][j];
 
	return(EXIT_SUCCESS);
}

int Reshape(const double *vec, double image[][NOCOL], const int m, const int n)
{
	for(int i=0; i<m; ++i)
		for(int j=0; j<n; ++j)
			image[i][j] = vec[i+j*n];

	return(EXIT_SUCCESS);
}

int GetPixelCenter2D(double *X, double *Y, const int m, const int n)
{
	for(int i=0; i<m; ++i)
		for(int j=0; j<n; ++j) {
			X[j+i*n] = i*1.0;
			Y[j+i*n] = (m-j-1)*1.0;
		}

	return(EXIT_SUCCESS);
}

int GenerateMotionData(double *Tx, double *Ty, const int nframes, const double deltax, const double deltay)
{
	for(int i=0; i<nframes; ++i) {
		Tx[i] = i*deltax;
		Ty[i] = i*deltay;
	}

	return(EXIT_SUCCESS);
}

int TransformCoordinates2D(double *Xnew, double *Ynew, const double Tx, const double Ty, const double *X, const double *Y, const int m, const int n)
{

	for(int i=0; i<m*n; ++i) {
		Xnew[i] = X[i] -Tx;
		Ynew[i] = Y[i] -Ty;
	}
	return(EXIT_SUCCESS);
}

int SpaceToCoordinates2D(const double *X, const double *Y, double *I, double *J, const int m, const int n)
{
	for(int i=0; i<m*n; ++i) {
		I[i] = m-1-Y[i];
		J[i] = X[i];
	}

	return(EXIT_SUCCESS);
}

int BuildInterpMatrix(const double *vecI, const double *vecJ, Epetra_Map map, Epetra_CrsMatrix *A, const int m, const int n)
{
	int myDimensions = map.NumMyElements();
	int *myGlobalDimensions = map.MyGlobalElements();
	int numEntries = 4;

	int *i0 = new int[m*n];
	int *i1 = new int[m*n];
	int *j0 = new int[m*n];
	int *j1 = new int[m*n];

	// 
	for(int index = 0; index < m*n; ++index) {
		i0[index] = static_cast<int>(floor(vecI[index]));
		i1[index] = i0[index] +1;
		j0[index] = static_cast<int>(floor(vecJ[index]));
		j1[index] = j0[index] +1;
	}

	//
	int rowIdxLength = 0;
	for (int index = 0; index < m*n; ++index) {
		if (i0[index]>=0 && i1[index]<m && j0[index] >= 0 && j1[index] < n) {
			rowIdxLength += 1;
		}
	}

	//
	int * rowIdx = new int [rowIdxLength];
	int indexForRowIdx = 0;
	
	for (int index = 0; index < m*n; ++index) {
		if (i0[index]>=0 && i1[index]<m && j0[index] >= 0 && j1[index] < n) {
			rowIdx[indexForRowIdx] = index;
			indexForRowIdx += 1;
		}
	}

	// compute the corresponding column index
	int *columnIdx1 = new int[rowIdxLength];
	int *columnIdx2 = new int[rowIdxLength];  
	int *columnIdx3 = new int[rowIdxLength];
	int *columnIdx4 = new int[rowIdxLength];
	
	for (int index = 0; index < rowIdxLength; ++index) {
		columnIdx1[index] = i0[rowIdx[index]] + m*(j0[rowIdx[index]]);
		columnIdx2[index] = i1[rowIdx[index]] + m*(j0[rowIdx[index]]);
		columnIdx3[index] = i0[rowIdx[index]] + m*(j1[rowIdx[index]]);
		columnIdx4[index] = i1[rowIdx[index]] + m*(j1[rowIdx[index]]);
	}

	// compute the weighting values
	double deltai, deltaj;
	double * weight1, *weight2, *weight3, *weight4;
	weight1 = new double [rowIdxLength];
	weight2 = new double [rowIdxLength];
	weight3 = new double [rowIdxLength];
	weight4 = new double [rowIdxLength];

	for (int index = 0; index < rowIdxLength; ++index) {
		deltai = vecI[rowIdx[index]] - i0[rowIdx[index]];
		deltaj = vecJ[rowIdx[index]] - j0[rowIdx[index]];
		weight1[index] = (1-deltai) * (1-deltaj);
		weight2[index] = (1-deltaj) * deltai;
		weight3[index] = (1-deltai) * deltaj;
		weight4[index] = (deltai) * deltaj;
	}

	// fill in the matrix data
	double *values = new double[4];
	int *indices = new int[4];
	for (int index = 0; index < myDimensions; ++index) {
		for (int indexForRowIdx = 0; indexForRowIdx < rowIdxLength; ++indexForRowIdx) {
			if (myGlobalDimensions[index] == rowIdx[indexForRowIdx]) {
				values[0] = weight1[indexForRowIdx];
				values[1] = weight2[indexForRowIdx];
				values[2] = weight3[indexForRowIdx];
				values[3] = weight4[indexForRowIdx];
				
				// global column indices
				indices[0] = columnIdx1[indexForRowIdx];
				indices[1] = columnIdx2[indexForRowIdx];
				indices[2] = columnIdx3[indexForRowIdx];
				indices[3] = columnIdx4[indexForRowIdx];
				(*A).InsertGlobalValues(myGlobalDimensions[index], numEntries, values, indices);
			}
		}
	}
	(*A).FillComplete();

	//	free memory;
	//delete vecI;
	//delete vecJ;
	delete[] i0;
	delete[] i1;
	delete[] j0;
	delete[] j1;

	delete[] rowIdx;
	delete[] weight1;
	delete[] weight2;
	delete[] weight3;
	delete[] weight4;

	delete[] columnIdx1;
	delete[] columnIdx2;
	delete[] columnIdx3;
	delete[] columnIdx4;
  
	delete[] values;
	delete[] indices;

	return(EXIT_SUCCESS);
}


int BuildInterpMatrix(const double *vecI, const double *vecJ, int *rowIdx, int *columnIdx, double *weight, const int m, const int n, const int rowIdxLength, const int frameIdx)
{
	
	int numEntries = 4;

	int *i0 = new int[m*n];
	int *i1 = new int[m*n];
	int *j0 = new int[m*n];
	int *j1 = new int[m*n];

	// 
	for(int index = 0; index < m*n; ++index) {
		i0[index] = static_cast<int>(floor(vecI[index]));
		i1[index] = i0[index] +1;
		j0[index] = static_cast<int>(floor(vecJ[index]));
		j1[index] = j0[index] +1;
	}


	//
	int indexForRowIdx = 0;
	for (int index = 0; index < m*n; ++index) {
		if (i0[index]>=0 && i1[index]<m && j0[index] >= 0 && j1[index] < n) {
			rowIdx[indexForRowIdx] = index;
			indexForRowIdx += 1;
		}
	}

	// compute the corresponding column index
	//int *columnIdx = new int[rowIdxLength*numEntries];
	
	for (int index = 0; index < rowIdxLength; ++index) {
		columnIdx[index*numEntries] = i0[rowIdx[index]] + m*(j0[rowIdx[index]]);
		columnIdx[index*numEntries +1] = i1[rowIdx[index]] + m*(j0[rowIdx[index]]);
		columnIdx[index*numEntries +2] = i0[rowIdx[index]] + m*(j1[rowIdx[index]]);
		columnIdx[index*numEntries +3] = i1[rowIdx[index]] + m*(j1[rowIdx[index]]);
	}

	// compute the weighting values
	double deltai, deltaj;
	//double * weight;
	//weight = new double [rowIdxLength*numEntries];

	for (int index = 0; index < rowIdxLength; ++index) {
		deltai = vecI[rowIdx[index]] - i0[rowIdx[index]];
		deltaj = vecJ[rowIdx[index]] - j0[rowIdx[index]];
		weight[index*numEntries] = (1-deltai) * (1-deltaj);
		weight[index*numEntries +1] = (1-deltaj) * deltai;
		weight[index*numEntries +2] = (1-deltai) * deltaj;
		weight[index*numEntries +3] = (deltai) * deltaj;
	}

	//	free memory;
	delete[] i0;
	delete[] i1;
	delete[] j0;
	delete[] j1;

	return(EXIT_SUCCESS);
}

int FindInterpMatrixLength(const double *vecI, const double *vecJ, const int m, const int n, int *rowIdxLength)
{
	
	int *i0 = new int[m*n];
	int *i1 = new int[m*n];
	int *j0 = new int[m*n];
	int *j1 = new int[m*n];

	// 
	for(int index = 0; index < m*n; ++index) {
		i0[index] = static_cast<int>(floor(vecI[index]));
		i1[index] = i0[index] +1;
		j0[index] = static_cast<int>(floor(vecJ[index]));
		j1[index] = j0[index] +1;
	}

	//
	*rowIdxLength = 0;
	for (int index = 0; index < m*n; ++index) {
		if (i0[index]>=0 && i1[index]<m && j0[index] >= 0 && j1[index] < n) {
			*rowIdxLength += 1;
		}
	}

	delete[] i0;
	delete[] i1;
	delete[] j0;
	delete[] j1;

	return(EXIT_SUCCESS);
}

int rest2D(int *iw, int *jw, double *vals, int *nnz, const int m, const int n, const int ssp)
{
	if(n%ssp != 0 || m%ssp != 0) {
		cout << "m or n must be multiple of ssp!" << endl;
		return(1);
	}
	
	*nnz = m*n;
	int idx = 0;

	for(int l=0; l<n/ssp; ++l)
		for(int i=0; i<m/ssp; ++i)
			for(int j=0; j<ssp; ++j)
				for(int k=0; k<ssp; ++k) {
					jw[idx] = l*m*ssp+i*ssp+j*m+k;
					iw[idx] = l*m/ssp+i;
					vals[idx] = 1.0/(ssp*ssp);
					idx += 1;
				}


	return(EXIT_SUCCESS);
}

int MakeMask(const int nf, const double r1, double *mask)
{
	double h = 2.0/nf;
	double *X1 = new double[nf*nf];
	double *X2 = new double[nf*nf];

	for(int i=0; i<nf; ++i)
		for(int j=0; j<nf; ++j) {
			X1[i+nf*j] = -1+i*h;
			X2[i+nf*j] = -1+j*h;
		}

	for(int i=0; i<nf*nf; ++i)
		mask[i] = sqrt(X1[i]*X1[i] + X2[i]*X2[i]);
	
	for(int i=0; i<nf*nf; ++i) {
		if(mask[i] <= r1) mask[i] = 1.0;
		else mask[i] = 0.0;
	}

	delete X1;
	delete X2;

	return(EXIT_SUCCESS);
}

int ConstructPupilWindowMatrixIndices(int *iw, int *jw, const int mf, const int nf, const int mLarge, const int nLarge)
{
	//int err;

	int *jw1 = new int[mf*nf];
	int *jw2 = new int[mf*nf];

	int nCompPad = ((mLarge-mf)/2);
	int t1 = mLarge*nCompPad + nCompPad;
	
	for(int i=0; i<nf; ++i){
		for(int j=0; j<mf; ++j) {
			jw1[i*mf+j] = mLarge*i;
			jw2[i*mf+j] = j + t1;
	}
	}

	for(int i=0; i<mf*nf; ++i) {
		iw[i] = i;
		jw[i] = jw1[i] + jw2[i];
	}

	delete[] jw1;
	delete[] jw2;

	return(EXIT_SUCCESS);
}

int ConvertWindVecs(double *deltax, double *deltay, const double *wind_vecs,const int nlayers, const int ssp){

	for (int i=0; i<nlayers; ++i){
		deltax[i] = ssp*wind_vecs[i]*cos(wind_vecs[i+nlayers]);
		deltay[i] = ssp*wind_vecs[i]*sin(wind_vecs[i+nlayers]);
	}
	
	return(EXIT_SUCCESS);
}

int GetCompositeImageSize(int *mLarge, int *nLarge, const int m, const int n, const double *deltax, const double *deltay,const int nframes,const int nlayers){

	int max_val = 0;
	//max over deltax
	for (int L=0;L<nlayers;++L){
		if(ceil(abs((deltax[L])*nframes)) > max_val){
			max_val = ceil(abs((deltax[L])*nframes));
		}
	}
	//max over deltay. we use two different loops in case they are different sizes, though they shouldn't be.
	for (int L=0;L<nlayers;++L){
		if(ceil(abs((deltay[L])*nframes)) > max_val){
			max_val = ceil(abs((deltay[L])*nframes));
		}
	}
	int n_comp_pad = max_val;
	int n_comp = n + 2*n_comp_pad;

	*mLarge = n_comp;
	*nLarge = n_comp;

	return(EXIT_SUCCESS);

}
int ConstructMotionMatrixIndices(int *rowIdx, int *colIdx, double *vals, const double *vecI, const double *vecJ, const int m, const int n, const int nframes, const int *rowIdxLength)

{		 

	// number of nonzeros per row
	int numEntries = 4;
	int err;

	// find the starting point of rowIdx, colIdx and vals for each frame
	int *start = new int[nframes];
	start[0] = 0;
	int preRowLength = rowIdxLength[0];
	for (int i=1; i<nframes; ++i) {
		start[i] = preRowLength;
		preRowLength += rowIdxLength[i];
	}

	for (int i=0; i<nframes; ++i) {
	   err = BuildInterpMatrix(&vecI[i*m*n], &vecJ[i*m*n], &rowIdx[start[i]], &colIdx[start[i]*numEntries], &vals[start[i]*numEntries], m, n, rowIdxLength[i], i);
	   if(err) return err;
	   for(int j=0; j<rowIdxLength[i]; ++j) {
		   rowIdx[start[i]+j] += i*m*n;
	   }
	}

   delete[] start;

   return(EXIT_SUCCESS);
}

int ConstructSparseDiagonalMatrixIndices(int *iwLarge, int *jwLarge, double *valsLarge, const int *iw, const int *jw, const double *vals, int nnz, int nframes, int m, int n)
{
	// m is the number of rows in each small sparse R or Wp.
	// n is the number of columns in each small sparse R or Wp.
	// nnz is the number of non-zeros per row in each small sparse R or Wp.
	// int *iwLarge = new int[nframes*nnz];
	// int *jwLarge = new int[nframes*nnz];
	// double *valsLarge = new double[nframes*nnz];
	
	for(int i=0; i<nframes; ++i)
		for(int j=0; j<nnz; ++j) {
			iwLarge[i*nnz+j] = iw[j] + i*m;
			jwLarge[i*nnz+j] = jw[j] + i*n;
			valsLarge[i*nnz+j] = vals[j];
		}

	return(EXIT_SUCCESS);

}
///////////////////// ADDED

int ConstructMotionMatrix(Epetra_CrsMatrix *&A, const int m, const int n, const int nframes, const int nlayers, const double *deltax, const double *deltay,Epetra_MpiComm &comm)
{
	int mLarge, nLarge;
	int err;
	err = GetCompositeImageSize(&mLarge, &nLarge, m, n, deltax, deltay,nframes,nlayers);

	// create the distributed sparse matrix
	int globalRowDimensions = mLarge*nLarge*nframes;
	int globalColDimensions = mLarge*nLarge*nlayers;

	Epetra_Map rowMap(globalRowDimensions, 0, comm);
	Epetra_Map colMap(globalColDimensions, 0, comm);

	// define the number of non-zeros per row
	int numEntries = 4;

	////////// Declaring all of the arrays using new
	/*
	double **Tx = new double*[nlayers];
	double **Ty = new double*[nlayers];

	double **X = new double*[nlayers];
	double **Y = new double*[nlayers];

	double **vecI = new double*[nlayers];
	double **vecJ = new double*[nlayers];

	int **rowIdxLength = new int*[nlayers];

	int *numRows = new int[nlayers];

	int **rowIdx = new int*[nlayers];
	int **colIdx = new int*[nlayers];
	double **vals = new double*[nlayers];

	double **values = new double*[nlayers];
	int **indices = new int*[nlayers];
	*/
	
	double* Tx = new double[nframes];
	double* Ty = new double[nframes];
	double* X = new double[mLarge*nLarge*nframes];
	double* Y = new double[mLarge*nLarge*nframes];
	double* vecI = new double[mLarge*nLarge*nframes];
	double* vecJ = new double[mLarge*nLarge*nframes];
	int* rowIdxLength = new int[nframes];
	int numRows;


	A = new Epetra_CrsMatrix(Copy, rowMap,numEntries*nlayers);
	
	for (int L=0; L<nlayers; ++L){
		// construct the large sparse motion matrix.
//		Tx[L] = new double[nframes];
//		Ty[L] = new double[nframes];

		// generate the motion data - wind velocity
		err = GenerateMotionData(Tx,Ty,nframes,deltax[L],deltay[L]);
		if(err) cout << "Failure to generate the motion data Tx and Ty" << endl;

		// generate coordinates
//		X[L] = new double[mLarge*nLarge*nframes];
//		Y[L] = new double[mLarge*nLarge*nframes];
		err = GetPixelCenter2D(X,Y,mLarge,nLarge);
		if(err) cout << "Failure to generate the coordinate system" << endl;

		// shift the coordinates
		for(int i=1; i<nframes; ++i) {
			err = TransformCoordinates2D(&X[i*mLarge*nLarge], &Y[i*mLarge*nLarge], Tx[i], Ty[i], X, Y, mLarge, nLarge);
			if(err) cout << "Failure to shift " << i+1 << "frame coordinates" << endl;
		}

		// convert coordinate system to array indices
		//vecI[L] = new double[mlarge*nlarge*nframes];
		//vecJ[L] = new double [mLarge*nLarge*nframes];
		for (int i=0; i<nframes; ++i) {
			err = SpaceToCoordinates2D(&X[i*mLarge*nLarge], &Y[i*mLarge*nLarge], &vecI[i*mLarge*nLarge], &vecJ[i*mLarge*nLarge], mLarge, nLarge);
		}

		// find the number of rows in the large interplation matrix
		//rowIdxLength[L] = new int[nframes];
		for (int i = 0; i<nframes; ++i) {
			err = FindInterpMatrixLength(&vecI[i*mLarge*nLarge], &vecJ[i*mLarge*nLarge], mLarge, nLarge, &rowIdxLength[i]);
		}

		numRows = 0;
		for (int i=0; i<nframes; ++i)
			numRows += rowIdxLength[i];

		// distribute memory for the total interpolation matrix A_i's
		int* rowIdx = new int[numRows];
		int* colIdx = new int[numRows*numEntries];
		double* vals = new double[numRows*numEntries];
		err = ConstructMotionMatrixIndices(rowIdx,colIdx,vals,vecI,vecJ,mLarge,nLarge,nframes,rowIdxLength);


		// fill in the matrix data
		double values[4];
		int indices[4];
		// shift the indices by mLarge*nLarge for each L so that each matrix is set to the right of the last
		int shift = mLarge*nLarge*L;

		for (int indexForRowIdx = 0; indexForRowIdx < numRows; ++indexForRowIdx) {
			values[0] = vals[indexForRowIdx*numEntries];
			values[1] = vals[indexForRowIdx*numEntries +1];
			values[2] = vals[indexForRowIdx*numEntries +2];
			values[3] = vals[indexForRowIdx*numEntries +3];
				
			// global column indices
			indices[0] = colIdx[indexForRowIdx*numEntries] + shift;
			indices[1] = colIdx[indexForRowIdx*numEntries +1] + shift;
			indices[2] = colIdx[indexForRowIdx*numEntries +2] + shift;
			indices[3] = colIdx[indexForRowIdx*numEntries +3] + shift;

			(*A).InsertGlobalValues(rowIdx[indexForRowIdx], numEntries, values, indices);
		}
		delete[] rowIdx;
		delete[] colIdx;
		delete[] vals;
	}

	(*A).FillComplete(colMap,rowMap);

/*
	for (int L=0; L<nlayers; ++L){
		delete[] Tx[L];
		delete[] Ty[L];
		delete[] X[L];
		delete[] Y[L];
		delete[] vecI[L];
		delete[] vecJ[L];
		delete[] rowIdxLength[L];
		delete[] rowIdx[L];
		delete[] colIdx[L];
		delete[] vals[L];
		delete[] values[L];
		delete[] indices[L];
	}

	delete[] Tx;
	delete[] Ty;
	delete[] X;
	delete[] Y;
	delete[] vecI;
	delete[] vecJ;
	delete[] rowIdxLength;
	delete[] rowIdx;
	delete[] colIdx;
	delete[] vals;
	delete[] values;
	delete[] indices;
	delete[] shift;
	delete[] numRows;
*/

	delete[] Tx;
	delete[] Ty;
	delete[] X;
	delete[] Y;
	delete[] vecI;
	delete[] vecJ;
	delete[] rowIdxLength;


	return(EXIT_SUCCESS);
}
	 
int ConstructDiagonalPupilMatrix(Epetra_CrsMatrix *& Wp,const int m, const int n, const int nframes, const int nlayers, const double *deltax, const double *deltay, const double *vals, Epetra_MpiComm &comm)
{

	int err; // for calling functions

	// compute the composite sizes of all frames: mLarge and nLarge
	int mLarge, nLarge;
	err = GetCompositeImageSize(&mLarge, &nLarge, m, n, deltax, deltay,nframes, nlayers);
	if(err) return err;

	// define indices for each small pupil mask
	int *iw = new int[m*n];
	int *jw = new int[m*n];

	err = ConstructPupilWindowMatrixIndices(iw, jw, m,n,mLarge,nLarge);
	if(err) return err;

	// define indices for large pupil mask
	int nnz = m*n;	// number of non-zeros per small mask
	int *iwLarge = new int[nframes*nnz];
	int *jwLarge = new int[nframes*nnz];
	double *valsLarge = new double[nframes*nnz];

	err = ConstructSparseDiagonalMatrixIndices(iwLarge, jwLarge, valsLarge, iw, jw, vals, nnz, nframes, m*n, mLarge*nLarge);
	if(err) return err;

	// define Epetra_CrsMatrix Wp: row and col dimensions
	int rowDimensions = m*n*nframes;
	int colDimensions = mLarge*nLarge*nframes;

	// define Epetra_CrsMatrix Wp: row and col map
	Epetra_Map rowMap(rowDimensions, 0, comm);
	Epetra_Map colMap(colDimensions, 0, comm);

	// define Epetra_CrsMatrix Wp: row local and global dimensions 
	int myRowDimensions = rowMap.NumMyElements();
	int *myRowGlobalDimensions = rowMap.MyGlobalElements();

	// define number of entries per row;
	int one = 1;

	//Wp = new Epetra_CrsMatrix(Copy, rowMap, colMap, one);
	Wp = new Epetra_CrsMatrix(Copy, rowMap, one);

	// fill in the matrix data
	for(int index=0; index<myRowDimensions; ++index) {
		int globalRowIdx = myRowGlobalDimensions[index];
		(*Wp).InsertGlobalValues(globalRowIdx,one,&valsLarge[globalRowIdx],&jwLarge[globalRowIdx]);
	}

	(*Wp).FillComplete(colMap,rowMap);

	delete[] iw;
	delete[] jw;
	//delete vals; //don't want to delete this because we need the pupil mask values later.
	delete[] iwLarge;
	delete[] jwLarge;
	delete[] valsLarge;

	return(EXIT_SUCCESS);

}

int ConstructDiagonalDownSampleMatrix(Epetra_CrsMatrix *& R,const int m,const int n,const int nframes, const int nlayers, const double *deltax,const double *deltay,const int ssp,Epetra_MpiComm &comm)
{
	int err; // for calling functions

	// compute the composite sizes: mLarge and nLarge
	int mLarge, nLarge;
	err = GetCompositeImageSize(&mLarge, &nLarge, m, n, deltax, deltay,nframes, nlayers);
	if(err) return err;

	// define the small downsampling matrix for each frame
	int *iw = new int[m*n];
	int *jw = new int[m*n];
	double *vals = new double[m*n];
	int nnz; // number of non-zeros per small R
	err = rest2D(iw, jw,vals, &nnz, m, n, ssp);
	if(err) return err;

	// define the indices for large composite R
	int *iwLarge = new int[nframes*nnz];
	int *jwLarge = new int[nframes*nnz];
	double *valsLarge = new double[nframes*nnz];
	err = ConstructSparseDiagonalMatrixIndices(iwLarge, jwLarge, valsLarge, iw, jw, vals, nnz, nframes, m*n/(ssp*ssp), m*n);
	if(err) return err;

	// define Epetra_CrsMatrix R: row and col dimensions
	int rowDimensions = m/ssp*n/ssp*nframes;
	int colDimensions = m*n*nframes;

	// define Epetra_CrsMatrix R: row and col map
	Epetra_Map rowMap(rowDimensions, 0, comm);
	Epetra_Map colMap(colDimensions, 0, comm);

	// define Epetra_CrsMatrix R: row local and global dimensions
	int myRowDimensions = rowMap.NumMyElements();
	int *myRowGlobalDimensions = rowMap.MyGlobalElements();

	// define number of entries per row
	int numEntries = ssp*ssp;

	//R = new Epetra_CrsMatrix(Copy, rowMap,colMap, numEntries);
	R = new Epetra_CrsMatrix(Copy, rowMap, numEntries);

	// fill in the matrix data
	for(int index=0; index<myRowDimensions; ++index) {
		int globalRowIdx = myRowGlobalDimensions[index];
		(*R).InsertGlobalValues(globalRowIdx,numEntries,&valsLarge[globalRowIdx*numEntries],&jwLarge[globalRowIdx*numEntries]);
	}

	(*R).FillComplete(colMap,rowMap);

	delete[] iw;
	delete[] jw;
//	delete[] vals; // want to keep these around
	delete[] iwLarge;
	delete[] jwLarge;
	delete[] valsLarge;

	return(EXIT_SUCCESS);
}

int ConstructPupilMotionMatrix(Epetra_CrsMatrix *&WpA, const int m, const int n, const int nframes, const int nlayers, const double *deltax, const double *deltay, const double *pupilVals, Epetra_MpiComm &comm)
{
	int mLarge, nLarge;
	int err;
	err = GetCompositeImageSize(&mLarge, &nLarge, m, n, deltax, deltay,nframes,nlayers);

	// create the distributed sparse matrix
	int globalRowDimensions = m*n*nframes;
	int globalColDimensions = mLarge*nLarge*nlayers;

	Epetra_Map rowMap(globalRowDimensions, 0, comm);
	Epetra_Map colMap(globalColDimensions, 0, comm);

	// define the number of non-zeros per row
	int numEntries = 4;

	////////// Declaring all of the arrays using new
	double **Tx = new double*[nlayers];
	double **Ty = new double*[nlayers];

	double **X = new double*[nlayers];
	double **Y = new double*[nlayers];

	double **vecI = new double*[nlayers];
	double **vecJ = new double*[nlayers];

	int **rowIdxLength = new int*[nlayers];

	int *numRows = new int[nlayers];

	int **rowIdx = new int*[nlayers];
	int **colIdx = new int*[nlayers];
	double **vals = new double*[nlayers];

	double **values = new double*[nlayers];
	int **indices = new int*[nlayers];

	int *shift = new int[nlayers];

	WpA = new Epetra_CrsMatrix(Copy, rowMap,numEntries*nlayers);

	// ADDING IN STUFF TO CALCULATE THE COLUMN INDICES OF W
	int *iw = new int[m*n];
	int *jw = new int[m*n];

	err = ConstructPupilWindowMatrixIndices(iw, jw, m,n,mLarge,nLarge);
	if(err) return err;

	// define indices for large pupil mask
	int nnz = m*n;	// number of non-zeros per small mask
	int *iwLarge = new int[nframes*nnz];
	int *jwLarge = new int[nframes*nnz];
	double *valsLarge = new double[nframes*nnz];

	err = ConstructSparseDiagonalMatrixIndices(iwLarge, jwLarge, valsLarge, iw, jw, pupilVals, nnz, nframes, m*n, mLarge*nLarge);
	if(err) return err;
	// END ADDING IN!!!
	
	for (int L=0; L<nlayers; ++L){
		// construct the large sparse motion matrix.
		Tx[L] = new double[nframes];
		Ty[L] = new double[nframes];

		// generate the motion data - wind velocity
		err = GenerateMotionData(Tx[L],Ty[L],nframes,deltax[L],deltay[L]);
		if(err) cout << "Failure to generate the motion data Tx and Ty" << endl;

		// generate coordinates
		X[L] = new double[mLarge*nLarge*nframes];
		Y[L] = new double[mLarge*nLarge*nframes];
		err = GetPixelCenter2D(X[L],Y[L],mLarge,nLarge);
		if(err) cout << "Failure to generate the coordinate system" << endl;

		// shift the coordinates
		for(int i=1; i<nframes; ++i) {
			err = TransformCoordinates2D(&X[L][i*mLarge*nLarge], &Y[L][i*mLarge*nLarge], Tx[L][i], Ty[L][i], X[L], Y[L], mLarge, nLarge);
			if(err) cout << "Failure to shift " << i+1 << "frame coordinates" << endl;
		}

		// convert coordinate system to array indices
		vecI[L] = new double[mLarge*nLarge*nframes];
		vecJ[L] = new double [mLarge*nLarge*nframes];
		for (int i=0; i<nframes; ++i) {
			err = SpaceToCoordinates2D(&X[L][i*mLarge*nLarge], &Y[L][i*mLarge*nLarge], &vecI[L][i*mLarge*nLarge], &vecJ[L][i*mLarge*nLarge], mLarge, nLarge);
		}

		// find the number of rows in the large interplation matrix
		rowIdxLength[L] = new int[nframes];
		for (int i = 0; i<nframes; ++i) {
			err = FindInterpMatrixLength(&vecI[L][i*mLarge*nLarge], &vecJ[L][i*mLarge*nLarge], mLarge, nLarge, &rowIdxLength[L][i]);
		}

		numRows[L] = 0;
		for (int i=0; i<nframes; ++i)
			numRows[L] += rowIdxLength[L][i];

		// distribute memory for the total interpolation matrix A_i's
		rowIdx[L] = new int[numRows[L]];
		colIdx[L] = new int[numRows[L]*numEntries];
		vals[L] = new double[numRows[L]*numEntries];
		err = ConstructMotionMatrixIndices(rowIdx[L],colIdx[L],vals[L],vecI[L],vecJ[L],mLarge,nLarge,nframes,rowIdxLength[L]);

		// fill in the matrix data
		values[L] = new double[4];
		indices[L] = new int[4];
		// shift the indices by mLarge*nLarge for each L so that each matrix is set to the right of the last
		shift[L] = mLarge*nLarge*L;
		
		// For each row in the motion matrix that we skip because the corresponding column of the pupil matrix is empty,
		// we need to ensure that we don't skip that row and leave it blank in Wp*A, but rather that we just skip it. To
		// do this, for each row that we skip, we subtract from the global row counter for A. So when we insert, we find 
		// what the row in A would normally be, then subtract the amount we wish to decrement the row number.
		int trueCtr = 0;
		for (int indexForRowIdx = 0; indexForRowIdx < numRows[L]; ++indexForRowIdx) {
			//cout << jwLarge[indexForRowIdx] << endl;
			// We want to ignore the rows that correspond to a completely empty column in the Window Matrix.
			if (std::binary_search(jwLarge,jwLarge+nframes*nnz,rowIdx[L][indexForRowIdx])){
					// Then we need to determine if a column that is not 'empty' corresponds to a
					// 0 value in the pupil matrix, if it does, skip this row, it is all zeros.
					if(valsLarge[trueCtr]!=0){
						values[L][0] = vals[L][indexForRowIdx*numEntries];
						values[L][1] = vals[L][indexForRowIdx*numEntries +1];
						values[L][2] = vals[L][indexForRowIdx*numEntries +2];
						values[L][3] = vals[L][indexForRowIdx*numEntries +3];
						
						// global column indices
						indices[L][0] = colIdx[L][indexForRowIdx*numEntries] + shift[L];
						indices[L][1] = colIdx[L][indexForRowIdx*numEntries +1] + shift[L];
						indices[L][2] = colIdx[L][indexForRowIdx*numEntries +2] + shift[L];
						indices[L][3] = colIdx[L][indexForRowIdx*numEntries +3] + shift[L];
					
						(*WpA).InsertGlobalValues(trueCtr, numEntries, values[L], indices[L]);
					}
					++trueCtr;
			}
		}	
	}

	(*WpA).FillComplete(colMap,rowMap);

	for (int L=0; L<nlayers; ++L){
		delete[] Tx[L];
		delete[] Ty[L];
		delete[] X[L];
		delete[] Y[L];
		delete[] vecI[L];
		delete[] vecJ[L];
		delete[] rowIdxLength[L];
		delete[] rowIdx[L];
		delete[] colIdx[L];
		delete[] vals[L];
		delete[] values[L];
		delete[] indices[L];
	}

	delete[] Tx;
	delete[] Ty;
	delete[] X;
	delete[] Y;
	delete[] vecI;
	delete[] vecJ;
	delete[] rowIdxLength;
	delete[] rowIdx;
	delete[] colIdx;
	delete[] vals;
	delete[] values;
	delete[] indices;
	delete[] shift;
	delete[] numRows;
	delete[] iw;
	delete[] jw;
	delete[] iwLarge;
	delete[] jwLarge;
	delete[] valsLarge;


	return(EXIT_SUCCESS);
}


int CalculateReguParamScale(double &lambdaScale, Epetra_CrsMatrix *&A){

	// Calculate regularization parameter scaling factor. We generate a bound on the 2-norm
	// using the one and infinity norms and exploting that our matrix A is always positive.
	
	int err;

	Epetra_MultiVector *onesA = new Epetra_MultiVector(A->DomainMap(),1);
	Epetra_MultiVector *onesAt = new Epetra_MultiVector(A->RangeMap(),1);
	Epetra_MultiVector *rowSums = new Epetra_MultiVector(A->RangeMap(),1);
	Epetra_MultiVector *colSums = new Epetra_MultiVector(A->DomainMap(),1);
	onesA->PutScalar(1.0);
	onesAt->PutScalar(1.0);
	err = A->Multiply(false,*onesA,*rowSums);
	if(err) return err;
	err = A->Multiply(true,*onesAt,*colSums);
	if(err) return err;
	double oneNormA;
	double infNormA;
	rowSums->MaxValue(&oneNormA);
	colSums->MaxValue(&infNormA);

	lambdaScale = sqrt(oneNormA*infNormA);

	delete onesA;
	delete onesAt;
	delete rowSums;
	delete colSums;

	return(EXIT_SUCCESS);
}

	

