#include "matio.h"
#include "EpetraExt_mmio.h"
#include "Epetra_Comm.h"
#include "Epetra_BlockMap.h"
#include "Epetra_Map.h"
#include "Epetra_Vector.h"
#include "Epetra_IntVector.h"
#include "Epetra_SerialDenseVector.h"
#include "Epetra_IntSerialDenseVector.h"
#include "Epetra_Import.h"
#include "Epetra_CrsMatrix.h"
#include <EpetraExt_ConfigDefs.h>


////////////////////////////////////// TO ARRAY?!?!?!
// Some Declarations to clear up some ordering.

int MultiVectorToArray(double **array, const Epetra_MultiVector & A, bool mmFormat);

// Internal functions

int writeMultiVectorToArray(double **array, const Epetra_MultiVector & A, bool mmFormat, int shift);



//////////////////////////////////////// READ ///////////////////////////////////////////////////////////


int ReadMatAsMultiVector(const char* filename, Epetra_Comm &comm, Epetra_MultiVector *& multiVec, Epetra_Map &map)
{
	/* Given a pointer to a multivector and information about the mat-file, this will read in the n-dimensional mat-file
	 * as a _vector_. At this point we don't have a true multivector. Will be added later if necessary.
	 */

	int err;

	mat_t *matArray;
	matvar_t *matVar;
	matArray = Mat_Open(filename,MAT_ACC_RDONLY);
	if (NULL == matArray)
	{
		err = -1;
		return err;
	}

	// get the info about the variable...	
	matVar = Mat_VarReadNext(matArray);
	if (NULL == matVar)
	{
		err = -1;
		return err;
	}

	// Need to cast matVar which is a null pointer to a char pointer...
	double *data = (double*)matVar->data;

	//Initialize the vector
	int numMyElements = map.NumMyElements();
	int* myGlobalElements = map.MyGlobalElements(); 
	
	int N = 1; // maybe N can change in the future
	multiVec = new Epetra_MultiVector(map, N); // 1 since only one col/vector for now
	double ** Ap = multiVec->Pointers();

	for (int j=0; j<N; ++j)
	{
		double * v = Ap[j];

		// Fill it
		for (int i=0; i<numMyElements; ++i)
		{
			v[i] = data[myGlobalElements[i]];
		}
	}

	Mat_VarFree(matVar);
	Mat_Close(matArray);

	return(EXIT_SUCCESS);
}

int ReadMatAsStdVector(const char* filename, double* array, int arraySize)
{
	/* Given a pointer of type double, read the mat file into a standard C++ type array */

	int err;

	mat_t *matArray;
	matvar_t *matVar;
	matArray = Mat_Open(filename,MAT_ACC_RDONLY);
	if (NULL == matArray)
	{
		err = -1;
		return err;
	}

	// get the info about the variable...	
	matVar = Mat_VarReadNext(matArray);
	if (NULL == matVar)
	{
		err = -1;
		return err;
	}

	// Need to cast matVar which is a null pointer to a char pointer...
	double *data = (double*)matVar->data;

	//Initialize the vector
	int N = 1;
	for (int j=0; j<N; ++j)
	{
		// Fill it
		for (int i=0; i<arraySize; ++i)
		{
			array[i] = data[i];
		}
	}

	Mat_VarFree(matVar);
	Mat_Close(matArray);

	return(EXIT_SUCCESS);
}

/////////////////////////////////// WRITE ////////////////////////////////////
int WriteMatArray(const char* filename, const char* varname, double *stdArray, int length)
{	

	// This one is for 1 dimensional arrays. See below for functions for higher dimensional arrays (2 and 3).	
	int err = 0;
	/* Given the multivector like the one above, this will write to a mat-file the data */
	
	mat_t *matfp;
	matvar_t *matVar;
	
	size_t dims[2] = {1, length}; //one vector of length length

	matfp = Mat_CreateVer(filename,NULL,MAT_FT_DEFAULT);
	if ( NULL == matfp )
	{
		cout << "Error creating MAT file " << filename << endl;
		err = -1;
		return err;
	}

	matVar = Mat_VarCreate(varname,MAT_C_DOUBLE,MAT_T_DOUBLE,2,dims,stdArray,0);
	if ( NULL == matVar )
	{
		cout << "Error creating variable " << endl;
		err = -1;
		return err;
	}
	else
	{
		Mat_VarWrite(matfp,matVar,MAT_COMPRESSION_NONE);
		Mat_VarFree(matVar);
		cout << "writing array!" << endl;
	}
	Mat_Close(matfp);

	return(EXIT_SUCCESS);
}


int WriteMatArray(const char* filename, const char* varname, double** stdArray, int* lengths)
{	

	/* This one is for 2 dimensional arrays.
	 * INPUTS
	 * 	- filename - obvious what this is I think
	 * 	- varname  - the name of the Matlab array variable
	 * 	- stdArray - normal C++ 2D array to be written
	 * 	- lengths  - array of length 2 giving the size of the array in each dimension.
	 */

	int err = 0;
	/* Given the multivector like the one above, this will write to a mat-file the data */
	
	// we have to create a 2D array where the data are stored contiguously.. so we need to copy it again...
	double contigArr[lengths[0]][lengths[1]]; // created on stack, should clear automagically
	for(int i=0;i<lengths[0];++i){
		for(int j=0;j<lengths[1];++j){
			contigArr[i][j] = stdArray[i][j];
		}
	}
	
	mat_t *matfp;
	matvar_t *matVar;
	
	size_t dims[2] = {lengths[0], lengths[1]}; //one vector of length length

	matfp = Mat_CreateVer(filename,NULL,MAT_FT_DEFAULT);
	if ( NULL == matfp )
	{
		cout << "Error creating MAT file " << filename << endl;
		err = -1;
		return err;
	}

	matVar = Mat_VarCreate(varname,MAT_C_DOUBLE,MAT_T_DOUBLE,2,dims,contigArr,0);
	if ( NULL == matVar )
	{
		cout << "Error creating variable " << endl;
		err = -1;
		return err;
	}
	else
	{
		Mat_VarWrite(matfp,matVar,MAT_COMPRESSION_NONE);
		Mat_VarFree(matVar);
		cout << "writing array!" << endl;
	}
	Mat_Close(matfp);

	return(EXIT_SUCCESS);
}

//////////////////////////////// TO ARRAY?!?!??! /////////////////////////////////

int MultiVectorToArray(double **array, const Epetra_MultiVector & A, bool mmFormat) {

	// Convert a distributed multivector to a local 2D array

  Epetra_BlockMap bmap = A.Map();
  const Epetra_Comm & comm = bmap.Comm();
  int numProc = comm.NumProc();

  if (numProc==1)
    writeMultiVectorToArray(array, A, mmFormat, 0); // only one vec, shift is 0
  else {

    // In the case of more than one column in the multivector, and writing to MatrixMarket
    // format, we call this function recursively, passing each vector of the multivector
    // individually so that we can get all of it written to file before going on to the next 
    // multivector
    if (A.NumVectors()>1 && mmFormat) {
      for (int i=0; i<A.NumVectors(); i++)
	if (MultiVectorToArray(array, *(A(i)), mmFormat)) return(-1);
      return(0);
    }

    Epetra_Map map(-1, bmap.NumMyPoints(), 0, comm);
    // Create a veiw of this multivector using a map (instead of block map)
    Epetra_MultiVector A1(View, map, A.Pointers(), A.NumVectors());
    int numRows = map.NumMyElements();
    
    Epetra_Map allGidsMap(-1, numRows, 0,comm);
    
    Epetra_IntVector allGids(allGidsMap);
    for (int i=0; i<numRows; i++) allGids[i] = map.GID(i);
    
    // Now construct a MultiVector on PE 0 by strip-mining the rows of the input matrix A.
    int numChunks = numProc;
    int stripSize = allGids.GlobalLength()/numChunks;
    int remainder = allGids.GlobalLength()%numChunks;
    int curStart = 0;
    int curStripSize = 0;
    int shift = 0;
    Epetra_IntSerialDenseVector importGidList;
    if (comm.MyPID()==0) 
      importGidList.Size(stripSize+1); // Set size of vector to max needed
    for (int i=0; i<numChunks; i++) {
      if (comm.MyPID()==0) { // Only PE 0 does this part
	curStripSize = stripSize;
	if (i<remainder) curStripSize++; // handle leftovers
	for (int j=0; j<curStripSize; j++) importGidList[j] = j + curStart;
	curStart += curStripSize;
      }
      // The following import map will be non-trivial only on PE 0.
      Epetra_Map importGidMap(-1, curStripSize, importGidList.Values(), 0, comm);
      Epetra_Import gidImporter(importGidMap, allGidsMap);
      Epetra_IntVector importGids(importGidMap);
      if (importGids.Import(allGids, gidImporter, Insert)) return(-1); 

      // importGids now has a list of GIDs for the current strip of matrix rows.
      // Use these values to build another importer that will get rows of the matrix.

      // The following import map will be non-trivial only on PE 0.
      Epetra_Map importMap(-1, importGids.MyLength(), importGids.Values(), 0, comm);
      Epetra_Import importer(importMap, map);
      Epetra_MultiVector importA(importMap, A1.NumVectors());
      if (importA.Import(A1, importer, Insert)) return(-1);
    

      // Finally we are ready to write this strip of the matrix to ostream
      if (writeMultiVectorToArray(array, importA, mmFormat, shift)) return(-1);
      shift += importA.GlobalLength();
    }
  }
  return(0);
}


int writeMultiVectorToArray(double **array, const Epetra_MultiVector & A, bool mmFormat, int shift) {

  int ierr = 0;
  int length = A.GlobalLength();
  int numVectors = A.NumVectors();
  const Epetra_Comm & comm = A.Map().Comm();
  if (comm.MyPID()!=0) {
    if (A.MyLength()!=0) ierr = -1;
  }
  else {
    if (length!=A.MyLength()) ierr = -1;
    for (int j=0; j<numVectors; j++) {
      for (int i=0; i<length; i++) {
	double val = A[j][i];
	//cout << "test idx j: " << j << endl;
	//cout << "test idx i+shift: " << i + shift << endl;
	if (mmFormat)
	  array[j][i + shift] = val;
	else{
	  array[j][i + shift] = val;
        }
      }
      //if (!mmFormat) fprintf(handle, "%s", "\n");
    }
  }
  int ierrGlobal;
  comm.MinAll(&ierr, &ierrGlobal, 1); // If any processor has -1, all return -1
  return(ierrGlobal);
}


int SaveMultiVector(const char* filename, const char* varname,  const Epetra_MultiVector & A){

	int err;
	double **std_ptr_arr = new double*[A.NumVectors()];
	for(int i=0;i<A.NumVectors();++i){
		std_ptr_arr[i] = new double[A.GlobalLength()];
	}
	err = MultiVectorToArray(std_ptr_arr, A, false);

	double std_arr[A.NumVectors()][A.GlobalLength()]; //put it into an array like this because it actually works this way.
	for(int j=0;j<A.NumVectors();++j){
		for(int i=0;i<A.GlobalLength();++i){
			std_arr[j][i] = std_ptr_arr[j][i];
		}
	}

	for(int i=0;i<A.NumVectors();++i){
		delete[] std_ptr_arr[i];
	}
	delete[] std_ptr_arr;

	if(err) return err;
	int sz[2];
	sz[1] = A.NumVectors();
	sz[0] = A.GlobalLength();
	if(A.Map().Comm().MyPID()==0){

		int err = 0;
		/* Given the multivector like the one above, this will write to a mat-file the data */
	
		mat_t *matfp;
		matvar_t *matVar;
	
		size_t dims[2] = {sz[0], sz[1]}; //one vector of length length
	
		matfp = Mat_CreateVer(filename,NULL,MAT_FT_DEFAULT);
		if ( NULL == matfp )
		{
			cout << "Error creating MAT file " << filename << endl;
			err = -1;
			return err;
		}

		matVar = Mat_VarCreate(varname,MAT_C_DOUBLE,MAT_T_DOUBLE,2,dims,std_arr,0);
		if ( NULL == matVar )
		{
			cout << "Error creating variable " << endl;
			err = -1;
			return err;
		}
		else
		{
			Mat_VarWrite(matfp,matVar,MAT_COMPRESSION_NONE);
			Mat_VarFree(matVar);
			cout << "writing array " << varname << endl;
		}
		Mat_Close(matfp);
	}

	return(EXIT_SUCCESS);


}

